namespace GeekStreamProject.Core.Tests
{/*
    public class ArticleTests
    {
        [Fact]
        public void ConstructorShouldFailWhenTitleIsNull()
        {
            int id = 1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = null;
            string description = "���������� �����";
            string content = "Cringe content";
            EMarks eMarks = EMarks.Like;
            Category category = Category.Science;
            List<Commentary> commentaries = new List<Commentary>();
            Review review = new Review(1, "Content", client, eMarks);

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, title, description, content, category, eMarks, client, commentaries, review);
            });
        }

        [Fact]
        public void ConstructorShouldFailWhenContentIsNull()
        {
            int id = 1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = "CringeTitle";
            string description = "���������� �����";
            string content = null;
            EMarks eMarks = EMarks.Like;
            Category category = Category.Science;
            List<Commentary> commentaries = new List<Commentary>();
            Review review = new Review(1, "Content", client, eMarks);
            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, title, description, content, category, eMarks, client, commentaries, review);
            });
        }

        [Fact]
        public void ConstructorShouldFailWhenCategoryIsUncorrect()
        {
            int id = 1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = "CringeTitle";
            string description = "���������� �����";
            string content = "CringeContent";
            EMarks eMarks = EMarks.Like;
            Category category = Category.None;
            List<Commentary> commentaries = new List<Commentary>();
            Review review = new Review(1, "Content", client, eMarks);

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, title, description, content, category, eMarks, client, commentaries, review);
            });
        }

        [Fact]
        public void ConstructorShouldFailWhenDescriptionIsUncorrect()
        {
            int id = 1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = "CringeTitle";
            string description = null;
            string content = "CringeContent";
            EMarks eMarks = EMarks.Like;
            Category category = Category.Games;
            List<Commentary> commentaries = new List<Commentary>();
            Review review = new Review(1, "Content", client, eMarks);

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, title, description, content, category, eMarks, client, commentaries, review);
            });
        }

        [Fact]
        public void ConstructorShouldFailWhenCommentraiesIsUncorrect()
        {
            int id = 1;
            Client client = new("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = "CringeTitle";
            string description = null;
            string content = "CringeContent";
            EMarks eMarks = EMarks.Like;
            Category category = Category.Games;
            List<Commentary> commentaries = null ;
            Review review = new Review(1, "Content", client, eMarks);

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, title, description, content, category, eMarks, client, commentaries, review);
            });
        }
    }*/
}
