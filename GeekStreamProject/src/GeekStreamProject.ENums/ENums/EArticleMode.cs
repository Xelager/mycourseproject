﻿namespace GeekStreamProject.ENums.ENums
{
    public enum EArticleMode
    {
        Published = 0,
        InProcessPublished = 1,
        UnderReview = 2,
        Draft = 3,
        Deleted = 4
    }
}
