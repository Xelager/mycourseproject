﻿namespace GeekStreamProject.ENums.ENums
{
    public enum ETypeOfMessage
    {
        ConfirmEmail = 0,
        ResetPassword = 1
    }
}
