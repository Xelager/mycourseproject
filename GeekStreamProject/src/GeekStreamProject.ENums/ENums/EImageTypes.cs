﻿namespace GeekStreamProject.ENums.ENums
{
    public enum EImageTypes
    {
        Logo = 0,
        UserAvatars = 1,
        Photos = 2,
        CategoryAvatars = 3,
        DefaultAvatar = 4
    }
}
