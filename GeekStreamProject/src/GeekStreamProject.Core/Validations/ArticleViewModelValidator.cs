﻿using FluentValidation;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.Validations
{
    public class ArticleViewModelValidator : AbstractValidator<CreateArticleViewModel>
    {
        public ArticleViewModelValidator()
        {
            this.CascadeMode = CascadeMode.Stop;

            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле")
                .Length(3, 150)
                .WithMessage("Заголовок должен содержать от 3 до 150 символов");

            RuleFor(x => x.Content)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле")
                .Must(x => x.Length > 300)
                .WithMessage("Текст статьи должен быть больше чем 300 символов");

            RuleFor(x => x.Description)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле")
                .Length(3, 300)
                .WithMessage("Описание статьи должно содержать от 3 до 300 символов");

            RuleFor(x => x.CategoryId)
                .Cascade(CascadeMode.Stop)
                .NotNull()
                .WithMessage("Необходимо заполнить поле");

            RuleFor(x => x.ImageViewModel)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

        }
    }
}
