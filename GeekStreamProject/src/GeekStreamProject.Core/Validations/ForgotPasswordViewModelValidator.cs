﻿using FluentValidation;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.Validations
{
    public class ForgotPasswordViewModelValidator : AbstractValidator<ForgotPasswordViewModel>
    {
        private readonly IUserService _userService;

        public ForgotPasswordViewModelValidator(IUserService userService)
        {
            _userService = userService;

            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");
        }
    }
}
