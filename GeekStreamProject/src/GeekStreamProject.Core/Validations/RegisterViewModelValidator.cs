﻿using FluentValidation;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.User;
using Microsoft.AspNetCore.Identity;

namespace GeekStreamProject.Core.Validations
{
    public class RegisterViewModelValidator : AbstractValidator<RegisterViewModel>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public RegisterViewModelValidator(IUserService userService, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            this.CascadeMode = CascadeMode.Stop;

            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage("Введите корректный e-mail");

            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

            When(x => !string.IsNullOrEmpty(x.Email), () =>
            {
                RuleFor(x => x.Email)
                    .Must(x => !userService.IsEmailExist(x).Result)
                    .WithMessage("Пользователь с таким email уже существует");
            });

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

            RuleFor(x => x.LastName)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

            RuleFor(x => x.Password)
                .Equal(x => x.ConfirmPassword)
                .When(x => !string.IsNullOrEmpty(x.Password) && !string.IsNullOrEmpty(x.ConfirmPassword))
                .WithMessage("Пароли не совпадают");

            RuleFor(x => x.Password)
                .MustAsync(async (pass, ct) => (await new PasswordValidator<ApplicationUser>()
                    .ValidateAsync(_userManager, null, pass)).Succeeded)
                .WithMessage("Пароль слишком лёгкий (не меньше 8 символов, цифры и буквы)");

            RuleFor(x => x.ConfirmPassword)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

            RuleFor(x => x.UserName)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

            RuleFor(x => x.UserName)
                .MustAsync(async (userName, _) => !await userService.IsUserNameExist(userName))
                .WithMessage("Пользователь с таким именем уже существует");
        }

    }
}
