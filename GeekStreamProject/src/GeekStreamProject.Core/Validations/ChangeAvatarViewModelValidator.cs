﻿using FluentValidation;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.Validations
{
    public class ChangeAvatarModelValidator : AbstractValidator<ChangeAvatarViewModel>
    {

        public ChangeAvatarModelValidator()
        {
            RuleFor(x => x.Avatar)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

        }
    }
}
