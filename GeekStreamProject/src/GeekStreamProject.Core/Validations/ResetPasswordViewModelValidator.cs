﻿using FluentValidation;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.User;
using Microsoft.AspNetCore.Identity;

namespace GeekStreamProject.Core.Validations
{
    public class ResetPasswordViewModelValidator : AbstractValidator<UpdatePasswordViewModel>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public ResetPasswordViewModelValidator(IUserService userService, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            this.CascadeMode = CascadeMode.Stop;

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

            RuleFor(x => x.Password)
                .Equal(x => x.ConfirmPassword)
                .When(x => !string.IsNullOrWhiteSpace(x.Password) && !string.IsNullOrWhiteSpace(x.ConfirmPassword))
                .WithMessage("Пароли не совпадают");

            RuleFor(x => x.Password)
                .MustAsync(async (pass, ct) => (await new PasswordValidator<ApplicationUser>()
                    .ValidateAsync(_userManager, null, pass)).Succeeded)
                .WithMessage("Пароль слишком лёгкий (не меньше 8 символов, цифры и буквы)");

            RuleFor(x => x.ConfirmPassword)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");
        }

    }
}
