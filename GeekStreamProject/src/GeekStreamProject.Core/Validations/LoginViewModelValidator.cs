﻿using FluentValidation;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.Validations
{
    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        private readonly IUserService _userService;
            
        public LoginViewModelValidator(IUserService userService)
        {
            _userService = userService;
            this.CascadeMode = CascadeMode.Stop;

            RuleFor(x => x.Email)
                .Must(x => !string.IsNullOrEmpty(x))
                .WithMessage("Необходимо заполнить поле");

            When(x => !string.IsNullOrEmpty(x.Email), () =>
            {
                RuleFor(x => x.Email)
                    .MustAsync(async (str, cancellationToken) => await _userService.IsEmailExist(str))
                    .WithMessage($"Такого аккаунта не существует");
            });

            RuleFor(x => x.Password)
                .Must(x => !string.IsNullOrEmpty(x))
                .WithMessage("Необходимо заполнить поле");

        }
    }
}
