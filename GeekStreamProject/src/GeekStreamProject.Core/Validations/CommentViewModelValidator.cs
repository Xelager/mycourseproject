﻿using FluentValidation;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.Validations
{
    public class CreateCommentViewModelValidator : AbstractValidator<CreateCommentViewModel>
    {
        public CreateCommentViewModelValidator()
        {
            this.CascadeMode = CascadeMode.Stop;

            RuleFor(x => x.Content)
                .NotEmpty()
                .WithMessage("Необходимо заполнить поле");

        }
    }
}
