﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.Subscription;
using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly ISubscriptionRepository _subscriptionRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IArticleService _articleService;

        public SubscriptionService(ISubscriptionRepository subscriptionRepository, IHttpContextAccessor httpContextAccessor,
            IArticleService articleService)
        {
            _subscriptionRepository = subscriptionRepository;
            _httpContextAccessor = httpContextAccessor;
            _articleService = articleService;
        }

        public async Task SubscribeToCategory(CreateSubscriptionViewModel model)
        {
            if (model is null)
            {
                throw new ArgumentException($"Argument {nameof(model)} shouldn't be 0");
            }

            var subscription = new Subscription()
            {
                SubscriberId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier),
                CategoryId = model.CategoryId
            };

            await _subscriptionRepository.Create(subscription);
        }

        public async Task SubscribeToAuthor(CreateSubscriptionViewModel model)
        {
            if (model is null)
            {
                throw new ArgumentException($"Argument {nameof(model)} shouldn't be 0");
            }

            var subscription = new Subscription()
            {
                SubscriberId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier),
                AuthorId = model.AuthorId
            };

            await _subscriptionRepository.Create(subscription);
        }

        public async Task<IEnumerable<SubscriptionViewModel>> SubscriptionsUser(string subId)
        {
            if (subId is null)
            {
                throw new ArgumentException($"Argument {nameof(subId)} shouldn't be 0");
            }
            IEnumerable<Subscription> listOfAuthors = await _subscriptionRepository.SubscriptionsUser(subId);
            if (listOfAuthors == null)
            {
                return null;
            }

            return listOfAuthors.Select(sub => new SubscriptionViewModel
            {
                Id = sub.Id,
                authorViewModel = new()
                {
                    Id = sub.Author.Id,
                    FirstName = sub.Author.FirstName,
                    LastName = sub.Author.LastName,
                    UserName = sub.Author.UserName,
                    Rating = sub.Author.Rating,
                    AvatarPath = sub?.Author?.Avatar?.FilePath,
                    CountArticle = sub.Author.Articles.Count,
                    articleViewModel = _articleService.GetLastArticleAuthor(sub.Author.Id).Result
                },
                subscriberViewModel = new()
                {
                    Id = sub.Subscriber.Id,
                    FirstName = sub.Subscriber.FirstName,
                    LastName = sub.Subscriber.LastName,
                    UserName = sub.Subscriber.UserName,
                    Rating = sub.Subscriber.Rating,
                    AvatarPath = sub?.Subscriber?.Avatar?.FilePath,
                    CountArticle = sub.Subscriber?.Articles?.Count,
                    articleViewModel = _articleService.GetLastArticleAuthor(sub.Subscriber.Id).Result
                },
                IsSigned = true,
                
            });
        }

        public async Task<IEnumerable<SubscriptionViewModel>> SubscribersUser(string subId)
        {
            if (subId is null)
            {
                throw new ArgumentException($"Argument {nameof(subId)} shouldn't be 0");
            }
            IEnumerable<Subscription> listOfAuthors = await _subscriptionRepository.SubscribersUser(subId);
            if (listOfAuthors == null)
            {
                return null;
            }

            return listOfAuthors.Select(sub => new SubscriptionViewModel
            {
                Id = sub.Id,
                authorViewModel = new()
                {
                    Id = sub.Author.Id,
                    FirstName = sub.Author.FirstName,
                    LastName = sub.Author.LastName,
                    UserName = sub.Author.UserName,
                    Rating = sub.Author.Rating,
                    AvatarPath = sub?.Author?.Avatar?.FilePath,
                    CountArticle = sub.Author.Articles.Count,
                    articleViewModel = _articleService.GetLastArticleAuthor(sub.Author.Id).Result
                },
                subscriberViewModel = new()
                {
                    Id = sub.Subscriber.Id,
                    FirstName = sub.Subscriber.FirstName,
                    LastName = sub.Subscriber.LastName,
                    UserName = sub.Subscriber.UserName,
                    Rating = sub.Subscriber.Rating,
                    AvatarPath = sub?.Subscriber?.Avatar?.FilePath,
                    CountArticle = sub?.Subscriber?.Articles?.Count,
                    articleViewModel = _articleService.GetLastArticleAuthor(sub.Subscriber.Id).Result
                },
                IsSigned = true

            });
        }

        public async Task<IEnumerable<SubscriptionViewModel>> GetSubOnCategories(string subId)
        {
            if (subId is null)
            {
                throw new ArgumentException($"Argument {nameof(subId)} shouldn't be 0");
            }
            IEnumerable<Subscription> listOfCategories = await _subscriptionRepository.SubOnCategories(subId);

            if (listOfCategories.Count() == 0)
            {
                return null;
            }

            return listOfCategories.Select(sub => new SubscriptionViewModel
            {
                Id = sub.Id,
                categoryViewModel = new()
                {
                    Id = sub.Category.Id,
                    Name = sub.Category.Name,
                    ImagePath = sub.Category?.Image?.FilePath,
                    CoverPath = sub.Category?.Cover?.FilePath
                },
                IsSigned = true,

            });


        }

        public async Task Unsubscribe(int subId)
        {
            await _subscriptionRepository.Delete(subId);
        }
    }
}
