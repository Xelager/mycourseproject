﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.Image;
using GeekStreamProject.Core.ViewModels.Subscription;
using GeekStreamProject.Core.ViewModels.User;
using GeekStreamProject.ENums.ENums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace GeekStreamProject.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IUsersRepository _userRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IEmailService _emailService;
        private readonly IArticleService _articleService;
        private readonly ISubscriptionService _subscriptionService;
        private readonly IImageService _imageService;

        public UserService(IUsersRepository userRepository, UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager, IEmailService emailService, IArticleService articleService,
            ISubscriptionService subscriptionService, IImageService imageService)
        {
            _userRepository = userRepository;
            _userManager = userManager;
            _roleManager = roleManager;
            _emailService = emailService;
            _articleService = articleService;
            _subscriptionService = subscriptionService;
            _imageService = imageService;
        }

        public async Task<IEnumerable<AuthorViewModel>> GetAllUsers()
        {
            IEnumerable<ApplicationUser> listOfAuthors = await _userRepository.GetAllAuthors();
            if (listOfAuthors is null)
            {
                return null;
            }

            return listOfAuthors.Select(author => new AuthorViewModel
            {
                Id = author.Id,
                UserName = author.UserName,
                FirstName = author.FirstName,
                LastName = author.LastName,
                Rating = author.Rating,
                AvatarPath = author?.Avatar?.FilePath,
                CountArticle = author.Articles.Where(x => x.ArticleMode == EArticleMode.Published).Count(),
                articleViewModel = _articleService.GetLastArticleAuthor(author.Id).Result
            });
        }

        public async Task<UserViewModel> GetUser(string userName)
        {
            var userProfile = await _userRepository.GetUserProfile(userName);
            if (userProfile is null)
            {
                return null;
            }

            var articleViewModels = await _articleService.GetArticlesByAuthor(userProfile.Id);
            var listOfSubscribers = await _subscriptionService.SubscribersUser(userProfile.Id);
            return new UserViewModel
            {
                Id = userProfile.Id,
                AvatarPath = userProfile.Avatar?.FilePath,
                UserName = userProfile.UserName,
                FirstName = userProfile.FirstName,
                LastName = userProfile.LastName,
                Rating = userProfile.Rating,
                Articles = articleViewModels,
                Subscribers = listOfSubscribers,
                AvatarUpdate = new ChangeAvatarViewModel() { Id = userProfile.Id},
                CreateSubscriptionViewModel = new CreateSubscriptionViewModel()
                {
                    AuthorId = userProfile.Id, AuthorName = userProfile.UserName
                }
            };
        }

        public async Task<bool> IsEmailExist(string email)
        {
            return await _userRepository.IsEmailExist(email);
        }

        public async Task<bool> IsUserNameExist(string userName)
        {
            return await _userRepository.IsUserNameExist(userName);
        }

        public async Task<bool> Register(RegisterViewModel model, Func<string, string, string> confirmAccount)
        {
            if (model == null)
            {
                return false;
            }

            var user = new ApplicationUser
            {
                AvatarId = model.Avatar?.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.UserName,
                Email = model.Email,
                Rating = 0,
                DateOfCreation = DateTime.Now
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return false;
            }

            await _userManager.AddToRoleAsync(user, "User");

            var userFromDb = await _userManager.FindByNameAsync(user.UserName);

            await _emailService.SendEmailAsync(userFromDb, "Подтверждение аккаунта на сайте GeekStream",
                confirmAccount);

            return true;
        }

        public async Task<bool> ResetPassword(ForgotPasswordViewModel model, Func<string, string, string> resetAccount)
        {
            if (model == null)
            {
                return false;
            }

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user is null)
            {
                return false;
            }

            var result = await _userManager.IsEmailConfirmedAsync(user);
            if (result)
            {
                await _emailService.SendResetPassword(user, "Сброс пароля на сайте GeekStream",
                    resetAccount);
            }
            else return false;

            return true;
        }

        public async Task<bool> UpdatePassword(UpdatePasswordViewModel model)
        {
            if (model == null)
            {
                return false;
            }

            var user = await _userManager.FindByIdAsync(model.userId);

            if (user is null)
            {
                return false;
            }

            var result = await _userManager.ResetPasswordAsync(user, model.token, model.Password);

            if (!result.Succeeded)
            {
                return false;
            }

            return true;
        }

        public async Task UpdateAvatar(ChangeAvatarViewModel changeAvatarViewModel)
        {
            if (changeAvatarViewModel == null)
            {
                return;
            }

            changeAvatarViewModel.Avatar.ImageType = EImageTypes.UserAvatars;
            int? imageId = await _imageService.CreateImage(changeAvatarViewModel.Avatar);
            if (imageId is null)
            {
                return;
            }
            var user = await _userManager.FindByIdAsync(changeAvatarViewModel.Id);
            user.AvatarId = imageId;
            await _userRepository.Update(user);

        }
    }
}
