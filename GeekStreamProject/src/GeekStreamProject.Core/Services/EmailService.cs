﻿using System;
using System.IO;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.User;
using GeekStreamProject.ENums.ENums;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MimeKit;

namespace GeekStreamProject.Core.Services
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;

        private static string s_messageTemplate = null;

        private readonly UserManager<ApplicationUser> _userManager;

        public EmailService(IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            _configuration = configuration;
            _userManager = userManager;
        }

        public async Task SendEmailAsync(ApplicationUser user, string subject, Func<string, string, string> confirmAccount)
        {
            string confirmationCode = await _userManager.GenerateEmailConfirmationTokenAsync(user);

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("GeekStreamProject", _configuration["Mail:Email"]));
            emailMessage.To.Add(new MailboxAddress("", user.Email));
            emailMessage.Subject = subject;
            string messageBody = ReadTemplate(ETypeOfMessage.ConfirmEmail).Replace("UserName", user.UserName).Replace("ActionUrl", confirmAccount(confirmationCode, user.Id));
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = messageBody
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_configuration["Mail:Host"], Convert.ToInt32(_configuration["Mail:Port"]), false);
                await client.AuthenticateAsync(_configuration["Mail:Email"], _configuration["Mail:Password"]);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }

        private string ReadTemplate(ETypeOfMessage type)
        {
            var dir = Directory.GetCurrentDirectory();
            if (string.IsNullOrEmpty(s_messageTemplate))
            {
                if (type == ETypeOfMessage.ConfirmEmail)
                {
                    using var reader = new StreamReader(_configuration["Mail:MailConfirmationTemplatePath"]);
                    s_messageTemplate = reader.ReadToEnd();
                }
                else if (type == ETypeOfMessage.ResetPassword)
                {
                    using var reader = new StreamReader(_configuration["Mail:MailResetPasswordTemplatePath"]);
                    s_messageTemplate = reader.ReadToEnd();
                }
            }

            return s_messageTemplate;
        }

        public async Task<bool> ConfirmAccount(string token, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new Exception("User not exist");
            var identityResult = await _userManager.ConfirmEmailAsync(user, token);
            return identityResult.Succeeded;
        }

        public async Task SendResetPassword(ApplicationUser user, string subject, Func<string, string, string> resetAccount)
        {
            string resetCode = await _userManager.GeneratePasswordResetTokenAsync(user);

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("GeekStreamProject", _configuration["Mail:Email"]));
            emailMessage.To.Add(new MailboxAddress("", user.Email));
            emailMessage.Subject = subject;
            string messageBody = ReadTemplate(ETypeOfMessage.ResetPassword).Replace("UserName", user.UserName).Replace("ActionUrl", resetAccount(resetCode, user.Id));
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = messageBody
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_configuration["Mail:Host"], Convert.ToInt32(_configuration["Mail:Port"]), false);
                await client.AuthenticateAsync(_configuration["Mail:Email"], _configuration["Mail:Password"]);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}
