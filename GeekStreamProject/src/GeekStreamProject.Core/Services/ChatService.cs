﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.Chat;
using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.Services
{
    public class ChatService : IChatService
    {
        private readonly IChatRepository _chatRepository;
        private readonly IChatMessageRepository _chatMessageRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ChatService(IChatRepository chatRepository, IChatMessageRepository chatMessageRepository,
            IHttpContextAccessor httpContextAccessor)
        {
            _chatRepository = chatRepository;
            _chatMessageRepository = chatMessageRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IEnumerable<MessageViewModel>> GetLatestMessages()
        {
            var listOfMessage = await _chatMessageRepository.GetLatestChatMessages();
            return listOfMessage
                .Select(message => new MessageViewModel
                {
                    Id = message.Id,
                    AuthorId = message.Author.Id,
                    Content = message.Content,
                    SendedDate = message.SendedDate,
                    AuthorName = message.Author.UserName,
                    AuthorAvatarUrl = message.Author.Avatar.FilePath
                });
        }

        public async Task Create(MessageViewModel model)
        {
            var message = new ChatMessage
            {
                AuthorId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier),
                Content = model.Content,
                SendedDate = DateTime.UtcNow
            };
            await _chatMessageRepository.Create(message);
        }

    }
}
