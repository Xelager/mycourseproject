﻿using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.ViewModels.Category;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IImageService _imageService;

        public CategoryService(ICategoryRepository categoryRepository, IImageService imageService)
        {
            _categoryRepository = categoryRepository;
            _imageService = imageService;
        }

        public async Task<IEnumerable<CategoryViewModel>> GetAllCategories()
        {
            IEnumerable<Category> listOfCategory = await _categoryRepository.GetCategories();
            return listOfCategory.Select(category => new CategoryViewModel
            {
                Id = category.Id,
                Name = category.Name,
                ImagePath = category.Image?.FilePath,
                CoverPath = category.Cover?.FilePath,
                CountArticles = category.Articles.Where(x => x.ArticleMode == EArticleMode.Published).Count(),
                CountSub = category.Subscribers.Count
            });
        }

        public async Task<CategoryViewModel> GetCategory(int id)
        {
            Category category = await _categoryRepository.GetOneCategory(id);

            if (category == null)
            {
                return null;
            }

            return new CategoryViewModel
            {
                Id = category.Id,
                Name = category.Name,
                ImagePath = category.Image?.FilePath,
                CoverPath = category.Cover?.FilePath,
                CountArticles = category.Articles.Where(x => x.ArticleMode == EArticleMode.Published).Count(),
                CountSub = category.Subscribers.Count()
            };
        }

        public async Task Create(CreateCategoryViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentException($"Argument {nameof(model)} shouldn't be 0");
            }

            int? imageId = null;
            int? coverId = null;
            if (model.ImageViewModel != null && model.CoverViewModel != null)
            {
                model.ImageViewModel.ImageType = EImageTypes.CategoryAvatars;
                model.CoverViewModel.ImageType = EImageTypes.CategoryAvatars;
                imageId = await _imageService.CreateImage(model.ImageViewModel);
                coverId = await _imageService.CreateImage(model.CoverViewModel);
            }
            var category = new Category()
            {
                Name = model.Name,
                ImageId = imageId,
                CoverId = coverId
            };

            await _categoryRepository.Create(category);
        }

        public async Task DeleteCategory(int id) => await _categoryRepository.DeleteCategoryCascade(id);
    }
}
