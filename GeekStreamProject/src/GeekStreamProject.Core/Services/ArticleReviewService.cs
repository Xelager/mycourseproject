﻿using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.ViewModels.Article;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.ENums.ENums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.ViewModels.Review;

namespace GeekStreamProject.Core.Services
{
    public class ArticleReviewService : IArticleReviewService
    {
        private readonly IArticleRepository _articleRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IArticleReviewRepository _articleReviewRepository;
        private readonly IArticleReviewerUserRepository _articleReviewerReviewerUserRepository;
        private readonly IUsersRepository _usersRepository;
        private readonly ICommentService _commentService;
        private readonly IApproveRepository _approveRepository;

        public ArticleReviewService(IArticleRepository articleRepository, UserManager<ApplicationUser> userManager,
            IArticleReviewRepository articleReviewRepository, IArticleReviewerUserRepository articleReviewerReviewerUserRepository,
            IUsersRepository usersRepository, ICommentService commentService, IApproveRepository approveRepository)
        {
            _articleRepository = articleRepository;
            _userManager = userManager;
            _articleReviewRepository = articleReviewRepository;
            _articleReviewerReviewerUserRepository = articleReviewerReviewerUserRepository;
            _usersRepository = usersRepository;
            _commentService = commentService;
            _approveRepository = approveRepository;
        }

        public async Task SendReviewToModerators(int? articleId)
        {
            var review = new ArticleReview() { ArticleId = articleId};
            int reviewId = await _articleReviewRepository.CreateReview(review);

            string roleName = "Moderator";
            var listOfModerator = await _usersRepository.GetUserInRole(roleName);

            if (listOfModerator != null)
            {
                foreach (ApplicationUserRole user in listOfModerator)
                {
                    var newReviewer = new ArticleReviewerUser() { UserId = user.UserId, ArticleReviewId = reviewId };
                    await _articleReviewerReviewerUserRepository.Create(newReviewer);
                }
            }
            
        }

        public async Task<IEnumerable<ReviewArticleViewModel>> GetAllReviewArticles()
        {
            IEnumerable<Article> listOfArticle = await _articleRepository.GetAllArticlesOnReview();
            if (listOfArticle is null)
            {
                return null;
            }
            return listOfArticle.Select(article => new ReviewArticleViewModel
            {
                Id = article.ArticleReview.Id,
                ArticleViewModel = new ArticleViewModel()
                {
                    Id = article.Id,
                    AuthorId = article.AuthorId,
                    Description = article.Description,
                    CategoryId = article.CategoryId,
                    Title = article.Title,
                    Content = article.Content,
                    CategoryName = article.Category.Name,
                    CategoryImagePath = article.Category.Image?.FilePath,
                    DateOfCreation = article.DateOfCreation,
                    Rating = article.Rating,
                    AuthorName = article.Author?.UserName,
                    AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                    ArticleReviewId = article.ArticleReview?.Id,
                    CommentCount = article.Comments?.Count,
                    Views = article.Views,
                    ImagePath = article.Image?.FilePath,
                    ArticleMode = article.ArticleMode,
                    publishViewModel = new() { ArticleId = article.Id }
                },
                CreateApproveViewModel = new CreateApproveViewModel()
                {
                    ArticleReviewId = article.ArticleReview.Id,
                    IsApproved = false
                }
            });
        }

        public async Task<IEnumerable<ReviewArticleViewModel>> GetMyReviewArticles(string authorId)
        {
            IEnumerable<Article> listOfArticle = await _articleRepository.GetMyArticlesOnReview(authorId);
            if (listOfArticle is null)
            {
                return null;
            }
            return listOfArticle.Select(article => new ReviewArticleViewModel
            {
                Id = article.ArticleReview.Id,
                ArticleViewModel = new ArticleViewModel()
                {
                    Id = article.Id,
                    AuthorId = article.AuthorId,
                    Description = article.Description,
                    CategoryId = article.CategoryId,
                    Title = article.Title,
                    Content = article.Content,
                    CategoryName = article.Category.Name,
                    CategoryImagePath = article.Category.Image?.FilePath,
                    DateOfCreation = article.DateOfCreation,
                    Rating = article.Rating,
                    AuthorName = article.Author?.UserName,
                    AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                    ArticleReviewId = article.ArticleReview?.Id,
                    CommentCount = article.Comments?.Count,
                    Views = article.Views,
                    ImagePath = article.Image?.FilePath,
                    ArticleMode = article.ArticleMode,
                    publishViewModel = new() { ArticleId = article.Id }
                },
                CreateApproveViewModel = new CreateApproveViewModel()
                {
                    ArticleReviewId = article.ArticleReview.Id,
                    IsApproved = false
                }
            });

        }

        public async Task<ReviewArticleViewModel> GetReviewArticle(int id)
        {
            Article article = await _articleRepository.GetOneArticle(id);

            if (article == null)
            {
                return null;
            }

            IEnumerable<ApproveViewModel> listOfApproveViewModel = null;
            var listOfApprove = await _approveRepository.GetApproves(article.ArticleReview.Id);
            if (listOfApprove != null)
            {
                listOfApproveViewModel = listOfApprove.Select(approve => new ApproveViewModel()
                {
                    Id = approve.Id,
                    IsApproved = approve.IsApproved,
                    ArticleReviewId = approve.ArticleReview.Id,
                    UserId = approve.UserId,
                    UserName = approve.User.UserName
                });
            }
            
            IEnumerable<CommentViewModel> listOfComments = await _commentService.GetReviewComments(id);
            return new ReviewArticleViewModel
            {
                Id = article.ArticleReview.Id,
                ApproveViewModels = listOfApproveViewModel,
                ArticleViewModel = new ArticleViewModel()
                {
                    Id = article.Id,
                    AuthorId = article.AuthorId,
                    CategoryId = article.CategoryId,
                    Description = article.Description,
                    Title = article.Title,
                    Content = article.Content,
                    CategoryName = article.Category.Name,
                    CategoryImagePath = article.Category.Image?.FilePath,
                    DateOfCreation = article.DateOfCreation,
                    Rating = article.Rating,
                    AuthorName = article.Author?.UserName,
                    AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                    ArticleReviewId = article.ArticleReview?.Id,
                    CommentCount = article.Comments?.Count,
                    CreateCommentViewModel = new CreateCommentViewModel() { ArticleReviewId = article.ArticleReview.Id, ArticleId = article.Id },
                    Comments = listOfComments,
                    Views = article.Views,
                    ImagePath = article.Image?.FilePath,
                    ArticleMode = article.ArticleMode,
                    publishViewModel = new() { ArticleId = article.Id }
                },
                CreateApproveViewModel = new CreateApproveViewModel()
                {
                    ArticleReviewId = article.ArticleReview.Id,
                    IsApproved = false
                }
            };
        }

        public async Task ApproveArticle(CreateApproveViewModel model)
        {
            if (model is null)
            {
                throw new ArgumentException($"Argument {nameof(model)} shouldn't be 0");
            }

            var approve = new Approve()
            {
                IsApproved = model.IsApproved,
                ArticleReviewId = model.ArticleReviewId,
                UserId = model.UserId
            };

            await _approveRepository.Create(approve);
            var article = await _articleRepository.GetArticleByReview(model.ArticleReviewId);
            if (article != null && article.ArticleMode == EArticleMode.UnderReview)
            {
                article.ArticleMode = EArticleMode.InProcessPublished;
                await _articleRepository.Update(article);
            }
        }

        

        public async Task UnApprove(int approveId) => await _approveRepository.Delete(approveId);
    }
}
