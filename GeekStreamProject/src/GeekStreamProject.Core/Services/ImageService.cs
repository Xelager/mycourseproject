﻿using System;
using System.IO;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.Image;
using GeekStreamProject.ENums.ENums;
using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.Services
{
    public class ImageService : IImageService
    {
        private readonly IImageRepository _imageRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ImageService(IImageRepository imageRepository, IHttpContextAccessor httpContextAccessor)
        {
            _imageRepository = imageRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<int> CreateImage(ImageViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentException($"Argument {nameof(model)} shouldn't be 0");
            }

            var image = new Image()
            {
                ImageType = model.ImageType,
            };

            return await _imageRepository.SaveImage(image, model.File);
        }

        public async Task<ImageViewModel> InstallDefaultAvatar()
        {
            var defaultAvatar = await _imageRepository.GetDefaultAvatar();
            ImageViewModel avatar = null;
            if (defaultAvatar is null)
            {
                IFormFile formFile;
                string path = "wwwroot/ImageStorage/Logo/GeekStreamProject_Logo.png";
                var stream = System.IO.File.OpenRead(path);
                formFile = new FormFile(stream, 0, stream.Length, null, Path.GetFileName(stream.Name));


                avatar = new ImageViewModel() { ImageType = EImageTypes.DefaultAvatar, File = formFile };

                avatar.Id = await CreateImage(avatar);
            }
            else
            {
                avatar = new ImageViewModel() { ImageType = defaultAvatar.ImageType, Id = defaultAvatar.Id };
            }

            return avatar;
        }
    }
}
