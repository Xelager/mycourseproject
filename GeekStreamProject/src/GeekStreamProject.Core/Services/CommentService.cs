﻿using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.ViewModels.Category;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.ENums.ENums;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using GeekStreamProject.Core.Abstractions.Services;
using Microsoft.AspNetCore.Http.Features;

namespace GeekStreamProject.Core.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CommentService(ICommentRepository commentRepository, IHttpContextAccessor httpContextAccessor)
        {
            _commentRepository = commentRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IEnumerable<CommentViewModel>> GetAllComments(int articleId)
        {
            IEnumerable<Comment> listOfComments = await _commentRepository.GetComments(articleId);
            return listOfComments.Select(comment => new CommentViewModel
            {
                Id = comment.Id,
                ArticleId = comment.ArticleId,
                Content = comment.Content,
                DateOfCreation = comment.DateOfCreation,
                Rating = comment.Rating,
                CommentatorName = comment.Commentator.UserName,
                CommentatorAvatarUrl = comment.Commentator.Avatar?.FilePath,
                ParentCommentContent = comment.ParentComment?.Content,
            });
        }
        public async Task<IEnumerable<CommentViewModel>> GetReviewComments(int articleId)
        {
            IEnumerable<Comment> listOfComments = await _commentRepository.GetReviewComments(articleId);
            return listOfComments.Select(comment => new CommentViewModel
            {
                Id = comment.Id,
                ArticleId = comment.ArticleId,
                Content = comment.Content,
                DateOfCreation = comment.DateOfCreation,
                Rating = comment.Rating,
                CommentatorName = comment.Commentator.UserName,
                CommentatorAvatarUrl = comment.Commentator.Avatar?.FilePath,
                ParentCommentContent = comment.ParentComment?.Content,
                ArticleReviewId = comment.ArticleReviewId
            });
        }

        public async Task Create(CreateCommentViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentException($"Argument {nameof(model)} shouldn't be 0");
            }

            var comment = new Comment()
            {
                CommentatorId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier),
                ArticleId = model.ArticleId,
                Content = model.Content,
                DateOfCreation = DateTime.UtcNow,
                Rating = 0,
                ParentCommentId = model.ParentCommentId,
                ArticleReviewId = model.ArticleReviewId ?? null
            };
            await _commentRepository.Create(comment);
        }

        public async Task DeleteComment(int id) => await _commentRepository.DeleteCommentCascade(id);
    }
}
