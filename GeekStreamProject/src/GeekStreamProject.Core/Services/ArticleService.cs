﻿using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.ViewModels.Article;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICommentService _commentService;
        private readonly IImageService _imageService;

        public ArticleService(IArticleRepository articleRepository, IHttpContextAccessor httpContextAccessor,
            ICommentService commentService, IImageService imageService)
        {
            _articleRepository = articleRepository;
            _httpContextAccessor = httpContextAccessor;
            _commentService = commentService;
            _imageService = imageService;
        }

        public async Task<IEnumerable<ArticleViewModel>> GetAllApprovedArticles()
        {
            IEnumerable<Article> listOfArticle = await _articleRepository.GetPublishedArticles();
            return listOfArticle.Select(article => new ArticleViewModel
            {
                Id = article.Id,
                AuthorId = article.AuthorId,
                CategoryId = article.CategoryId,
                Description = article.Description,
                Title = article.Title,
                Content = article.Content,
                CategoryName = article.Category.Name,
                CategoryImagePath = article.Category.Image?.FilePath,
                DateOfCreation = article.DateOfCreation,
                Rating = article.Rating,
                AuthorName = article.Author?.UserName,
                AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                ArticleReviewId = article.ArticleReview?.Id,
                CommentCount = article.Comments.Where(x => x.ArticleReviewId == null).Count(),
                ReviewComments = _commentService.GetReviewComments(article.Id).Result,
                Views = article.Views,
                ImagePath = article.Image?.FilePath,
                ArticleMode = article.ArticleMode,
                publishViewModel = new() { ArticleId = article.Id }
            });
        }

        public async Task<IEnumerable<ArticleViewModel>> GetArticlesByAuthor(string authorId)
        {
            IEnumerable<Article> listOfArticle = await _articleRepository.GetArticlesByAuthor(authorId);
            return listOfArticle.Select(article => new ArticleViewModel
            {
                Id = article.Id,
                AuthorId = article.AuthorId,
                CategoryId = article.CategoryId,
                Description = article.Description,
                Title = article.Title,
                Content = article.Content,
                CategoryName = article.Category.Name,
                CategoryImagePath = article.Category.Image?.FilePath,
                DateOfCreation = article.DateOfCreation,
                Rating = article.Rating,
                AuthorName = article.Author?.UserName,
                AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                ArticleReviewId = article.ArticleReview?.Id,
                CommentCount = article.Comments.Where(x => x.ArticleReviewId == null).Count(),
                ReviewComments = _commentService.GetReviewComments(article.Id).Result,
                Views = article.Views,
                ImagePath = article.Image?.FilePath,
                ArticleMode = article.ArticleMode,
                publishViewModel = new() { ArticleId = article.Id }
            });
        }

        public async Task<ArticleViewModel> GetArticle(int id)
        {
            Article article = await _articleRepository.GetOneArticle(id);

            if (article == null)
            {
                return null;
            }
            IEnumerable<CommentViewModel> listOfComments = await _commentService.GetAllComments(id);
            return new ArticleViewModel
            {
                Id = article.Id,
                AuthorId = article.AuthorId,
                CategoryId = article.CategoryId,
                Description = article.Description,
                Title = article.Title,
                Content = article.Content,
                CategoryName = article.Category.Name,
                CategoryImagePath = article.Category.Image?.FilePath,
                DateOfCreation = article.DateOfCreation,
                Rating = article.Rating,
                AuthorName = article.Author?.UserName,
                AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                ArticleReviewId = article.ArticleReview?.Id,
                CommentCount = article.Comments.Where(x => x.ArticleReviewId == null).Count(),
                CreateCommentViewModel = new CreateCommentViewModel() { ArticleReviewId = null, ArticleId = article.Id },
                ReviewComments = _commentService.GetReviewComments(article.Id).Result,
                Comments = listOfComments,
                Views = article.Views,
                ImagePath = article.Image?.FilePath,
                ArticleMode = article.ArticleMode,
                publishViewModel = new() { ArticleId = article.Id }
            };
        }

        public async Task<ArticleViewModel> GetArticleByReview(int id)
        {
            Article article = await _articleRepository.GetArticleByReview(id);

            if (article == null)
            {
                return null;
            }
            IEnumerable<CommentViewModel> listOfComments = await _commentService.GetReviewComments(id);
            return new ArticleViewModel
            {
                Id = article.Id,
                AuthorId = article.AuthorId,
                CategoryId = article.CategoryId,
                Description = article.Description,
                Title = article.Title,
                Content = article.Content,
                CategoryName = article.Category.Name,
                CategoryImagePath = article.Category.Image?.FilePath,
                DateOfCreation = article.DateOfCreation,
                Rating = article.Rating,
                AuthorName = article.Author?.UserName,
                AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                ArticleReviewId = article.ArticleReview?.Id,
                CommentCount = listOfComments.Count(),
                CreateCommentViewModel = new CreateCommentViewModel() { ArticleReviewId = null, ArticleId = article.Id },
                Comments = listOfComments,
                Views = article.Views,
                ImagePath = article.Image?.FilePath,
                ArticleMode = article.ArticleMode,
                publishViewModel = new() { ArticleId = article.Id }
            };
        }

        public async Task<IEnumerable<ArticleViewModel>> GetArticlesByCategory(int categoryId)
        {
            IEnumerable<Article> listOfArticle = await _articleRepository.GetArticlesByCategory(categoryId);
            if (listOfArticle.Count() == 0)
            {
                return null;
            }
            return listOfArticle.Select(article => new ArticleViewModel
            {
                Id = article.Id,
                AuthorId = article.AuthorId,
                Description = article.Description,
                CategoryId = article.CategoryId,
                Title = article.Title,
                Content = article.Content,
                CategoryName = article.Category.Name,
                CategoryImagePath = article.Category.Image?.FilePath,
                DateOfCreation = article.DateOfCreation,
                Rating = article.Rating,
                CategoryCoverPath = article.Category.Cover.FilePath,
                AuthorName = article.Author?.UserName,
                AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                ArticleReviewId = article.ArticleReview?.Id,
                CommentCount = article.Comments.Where(x => x.ArticleReviewId == null).Count(),
                ReviewComments = _commentService.GetReviewComments(article.Id).Result,
                Views = article.Views,
                ImagePath = article.Image?.FilePath,
                ArticleMode = article.ArticleMode,
                publishViewModel = new() { ArticleId = article.Id }
            });
        }

        public async Task<IEnumerable<ArticleViewModel>> GetMyDrafts(string authorId)
        {
            IEnumerable<Article> listOfArticle = await _articleRepository.GetDraftArticles(authorId);
            if (listOfArticle is null)
            {
                return null;
            }
            return listOfArticle.Select(article => new ArticleViewModel
            {
                Id = article.Id,
                AuthorId = article.AuthorId,
                Description = article.Description,
                CategoryId = article.CategoryId,
                Title = article.Title,
                Content = article.Content,
                CategoryName = article.Category.Name,
                CategoryImagePath = article.Category.Image?.FilePath,
                DateOfCreation = article.DateOfCreation,
                Rating = article.Rating,
                AuthorName = article.Author?.UserName,
                AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                ArticleReviewId = article.ArticleReview?.Id,
                CommentCount = article.Comments?.Count,
                Views = article.Views,
                ImagePath = article.Image?.FilePath,
                ArticleMode = article.ArticleMode,
                publishViewModel = new() { ArticleId = article.Id }
            });
        }

        public async Task<int> Create(CreateArticleViewModel model)
        {
            string userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (userId == null)
            {
                throw new ArgumentException($"User should be authenticated");
            }

            int? imageId = null;
            if (model.ImageViewModel?.File != null)
            {
                model.ImageViewModel.ImageType = EImageTypes.Photos;
                imageId = await _imageService.CreateImage(model.ImageViewModel);
 
            }

            var article = new Article()
            {
                Title = model.Title,
                Content = model.Content,
                Description = model.Description,
                Views = 0,
                Rating = 0,
                ArticleMode = model.ArticleMode,
                DateOfCreation = DateTime.UtcNow,
                AuthorId = userId,
                CategoryId = model.CategoryId,
                ImageId = imageId
            };

            return await _articleRepository.CreateArticle(article);
        }

        public async Task<ArticleViewModel> GetLastArticleAuthor(string authorId)
        {
            Article article = await _articleRepository.GetLastArticleAuthor(authorId);

            if (article is null)
            {
                return null;
            }
                

            return new ArticleViewModel()
            {
                Id = article.Id,
                Title = article.Title,
                DateOfCreation = article.DateOfCreation,
                ArticleMode = article.ArticleMode
            };
        }

        public async Task<IEnumerable<ArticleViewModel>> GetMyNotPublishedArticles(string authorId)
        {
            IEnumerable<Article> listOfArticle = await _articleRepository.GetNotPublishedArticles(authorId);
            if (listOfArticle is null)
            {
                return null;
            }
            return listOfArticle.Select(article => new ArticleViewModel
            {
                Id = article.Id,
                AuthorId = article.AuthorId,
                Description = article.Description,
                CategoryId = article.CategoryId,
                Title = article.Title,
                Content = article.Content,
                CategoryName = article.Category.Name,
                CategoryImagePath = article.Category.Image?.FilePath,
                DateOfCreation = article.DateOfCreation,
                Rating = article.Rating,
                AuthorName = article.Author?.UserName,
                AuthorAvatarPath = article.Author?.Avatar?.FilePath,
                ArticleReviewId = article.ArticleReview?.Id,
                CommentCount = article.Comments.Where(x => x.ArticleReviewId == null).Count(),
                ReviewComments = _commentService.GetReviewComments(article.Id).Result,
                Views = article.Views,
                ImagePath = article.Image?.FilePath,
                ArticleMode = article.ArticleMode,
                publishViewModel = new() { ArticleId = article.Id }
            });
        }

        public async Task ToDraft(int articleId)
        {
            await _articleRepository.AddToDraft(articleId);
        }

        public async Task ToPublish(CreatePublishViewModel model)
        {
            if (model is null)
            {
                throw new ArgumentException($"Argument {nameof(model)} shouldn't be 0");
            }

            var article = await _articleRepository.GetOneArticle(model.ArticleId);
            if (article != null)
            {
                article.DateOfCreation = DateTime.UtcNow;
                article.ArticleMode = model.ArticleMode;
                await _articleRepository.Update(article);
            }
        }
        public async Task DeleteArticle(int id)
        {
            await _articleRepository.DeleteArticleCascade(id);
        }

        public async Task ChangeRating(int id, int delta)
        {
            await _articleRepository.ChangeRatingArticle(id, delta);
        }
    }
}
