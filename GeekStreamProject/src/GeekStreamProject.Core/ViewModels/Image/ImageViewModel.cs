using GeekStreamProject.ENums.ENums;
using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.ViewModels.Image
{
    public class ImageViewModel
    {
        public int Id { get; set; }

        public IFormFile File { get; set; }

        public EImageTypes ImageType { get; set; }

        public int? ArticleId { get; set; }

        public int? CategoryId { get; set; }

        public string UserId { get; set; }
    }
}
