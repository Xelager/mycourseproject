﻿using System.Collections.Generic;
using GeekStreamProject.Core.ViewModels.Article;

namespace GeekStreamProject.Core.ViewModels.Tag
{
    public class TagViewModel
    {
        public int Id { get; set; }

        public string WorldOfTag { get; set; }

        public int ArticleId { get; set; }
    }
}
