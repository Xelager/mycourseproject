﻿using System;
using GeekStreamProject.ENums.ENums;
using System.ComponentModel.DataAnnotations;
using System.IO;
using GeekStreamProject.Core.ViewModels.Image;
using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.ViewModels.User
{
    public class RegisterViewModel
    {
        public ImageViewModel Avatar { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
