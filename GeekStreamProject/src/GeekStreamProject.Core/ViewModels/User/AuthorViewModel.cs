﻿using System;
using System.Collections.Generic;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.Subscription;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.ViewModels.User
{
    public class AuthorViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public int? Rating { get; set; }

        public string AvatarPath { get; set; }

        public int? CountArticle { get; set; }

        public ArticleViewModel articleViewModel { get; set; }
    }
}
