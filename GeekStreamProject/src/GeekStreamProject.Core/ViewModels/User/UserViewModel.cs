﻿using System;
using System.Collections.Generic;
using System.Threading;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.Subscription;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.ViewModels.User
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? Rating { get; set; }

        public string AvatarPath { get; set; }

        public string UserName { get; set; }

        public DateTime DateOfCreation { get; set; }

        public IEnumerable<ArticleViewModel> Articles { get; set; }

        public IEnumerable<SubscriptionViewModel> Subscribers { get; set; }

        public ChangeAvatarViewModel AvatarUpdate { get; set; }

        public CreateSubscriptionViewModel CreateSubscriptionViewModel { get; set; }

       
    }
}
