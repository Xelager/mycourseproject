﻿using System.ComponentModel.DataAnnotations;

namespace GeekStreamProject.Core.ViewModels.User
{
    public class UpdatePasswordViewModel
    {
        public string userId { get; set; }

        public string token { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
