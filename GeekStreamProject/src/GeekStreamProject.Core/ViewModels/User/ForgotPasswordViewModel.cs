﻿using System.ComponentModel.DataAnnotations;

namespace GeekStreamProject.Core.ViewModels.User
{
    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
    }
}
