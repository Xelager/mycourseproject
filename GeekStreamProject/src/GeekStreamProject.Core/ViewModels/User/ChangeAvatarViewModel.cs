﻿using System;
using System.Collections.Generic;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.Image;
using GeekStreamProject.Core.ViewModels.Subscription;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.ViewModels.User
{
    public class ChangeAvatarViewModel
    {
        public string Id { get; set; }

        public ImageViewModel Avatar { get; set; }
    }
}
