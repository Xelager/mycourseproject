using System;
using System.Collections.Generic;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.ViewModels.Review
{
    public class CreateApproveViewModel
    {
        public int Id { get; set; }

        public bool IsApproved { get; set; }

        public int ArticleReviewId { get; set; } 

        public string UserId { get; set; }
    }
}
