using System;
using System.Collections.Generic;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.ViewModels.Review
{
    public class ReviewArticleViewModel
    {
        public int? Id { get; set; }
        public ArticleViewModel ArticleViewModel { get; set; }

        public IEnumerable<ApproveViewModel> ApproveViewModels { get; set; }

        public CreateApproveViewModel CreateApproveViewModel { get; set; }
}
}
