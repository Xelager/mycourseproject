using System;
using System.Collections.Generic;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.ViewModels.Comment
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        public int? ArticleId { get; set; }

        public string Content { get; set; }

        public string ParentCommentContent { get; set; }

        public string CommentatorName { get; set; }

        public string CommentatorAvatarUrl { get; set; }

        public DateTime DateOfCreation { get; set; }

        public int? Rating { get; set; }

        public int? ArticleReviewId { get; set; }

        public UserViewModel Commentator { get; set; }

        public IEnumerator<CommentViewModel> ChildComments { get; set; }

        public CreateCommentViewModel CreateViewModel { get; set; }
    }
}
