using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.ViewModels.Comment
{
    public class CreateCommentViewModel
    {
        public int Id { get; set; }

        public int? ArticleId { get; set; }

        public int? ParentCommentId { get; set; }

        public string CommentatorId { get; set; }

        [DataType(DataType.MultilineText)]
        [Required]
        public string Content { get; set; }

        public int? Rating { get; set; }

        public int? ArticleReviewId { get; set; }

        public UserViewModel Commentator { get; set; }

        public ICollection<CommentViewModel> ChildComments { get; set; }

        public bool articleReview { get; set; } = false;
    }
}
