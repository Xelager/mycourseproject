using GeekStreamProject.Core.ViewModels.Subscription;

namespace GeekStreamProject.Core.ViewModels.Category
{
    public class CategoryViewModel
    {
        public int Id { get; set; }

        public CreateSubscriptionViewModel CreateSubscriptionViewModel { get; set; }

        public string Name { get; set; }

        public string ImagePath { get; set; }

        public string CoverPath { get; set; }

        public int? CountArticles { get; set; }

        public int? CountSub { get; set; }
    }
}
