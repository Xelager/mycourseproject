using GeekStreamProject.Core.ViewModels.Image;
using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.ViewModels.Category
{
    public class CreateCategoryViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ImageViewModel CoverViewModel { get; set; }

        public ImageViewModel ImageViewModel { get; set; }
    }
}
