﻿

namespace GeekStreamProject.Core.ViewModels.Subscription
{
    public class CreateSubscriptionViewModel
    {
        public int Id { get; set; }

        public string SubscriberId { get; set; }

        public string AuthorId { get; set; }

        public int CategoryId { get; set; }

        public string AuthorName { get; set; }

        public bool IsSigned { get; set; } = false;
    }
}
