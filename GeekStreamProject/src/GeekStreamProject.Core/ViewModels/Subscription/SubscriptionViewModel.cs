﻿

using System.Collections.Generic;
using GeekStreamProject.Core.ViewModels.Category;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.ViewModels.Subscription
{
    public class SubscriptionViewModel
    {
        public int Id { get; set; }
        public AuthorViewModel subscriberViewModel { get; set; }

        public AuthorViewModel authorViewModel { get; set; }

        public CategoryViewModel categoryViewModel { get; set; }

        public bool IsSigned { get; set; } = false;
    }
}
