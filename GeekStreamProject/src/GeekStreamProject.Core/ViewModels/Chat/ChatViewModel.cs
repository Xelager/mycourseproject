﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Core.ViewModels.Chat
{
    public class ChatViewModel
    {
        public string AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string AuthorAvatar { get; set; }

        public MessageViewModel LastMessage { get; set; }

        public ICollection<MessageViewModel> ListOfMessage { get; set; }

        public DateTime SendedDate { get; set; }
    }
}
