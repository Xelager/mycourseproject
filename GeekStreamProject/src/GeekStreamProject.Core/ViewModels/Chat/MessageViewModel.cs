﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Core.ViewModels.Chat
{
    public class MessageViewModel 
    {
        public int Id { get; set; }

        public string AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string AuthorAvatarUrl { get; set; }

        public string Content { get; set; }

        public DateTime SendedDate { get; set; }
    }
}
