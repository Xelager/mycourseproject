using System;
using System.Collections.Generic;
using System.Security.Permissions;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.Core.ViewModels.Image;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.ViewModels.Article
{
    public class ArticleViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImagePath { get; set; }

        public string Content { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string CategoryCoverPath { get; set; }

        public string CategoryImagePath { get; set; }

        public DateTime DateOfCreation { get; set; }

        public int? Rating { get; set; }

        public string AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string AuthorAvatarPath { get; set; }

        public int? ArticleReviewId { get; set; }

        public int? CommentCount { get; set; }

        public int? Views { get; set; }

        public IEnumerable<CommentViewModel> Comments { get; set; }

        public IEnumerable<CommentViewModel> ReviewComments { get; set; }

        public CreateCommentViewModel CreateCommentViewModel { get; set; }

        public CommentViewModel CommentViewModel { get; set; }

        public CreatePublishViewModel publishViewModel { get; set; }

        public EArticleMode ArticleMode { get; set; }
    }
}
