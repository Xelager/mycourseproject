using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GeekStreamProject.Core.ViewModels.Category;
using GeekStreamProject.Core.ViewModels.Image;
using GeekStreamProject.Core.ViewModels.Tag;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.ViewModels.Article
{
    public class CreateArticleViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        public ImageViewModel ImageViewModel { get; set; }

        public int? CategoryId { get; set; }

        public DateTime DateOfCreation { get; set; }

        public int? Rating { get; set; }

        public string AuthorId { get; set; }

        public int? ArticleReviewId { get; set; }

        public int? CommentCount { get; set; }

        public int? Views { get; set; }

        public EArticleMode ArticleMode { get; set; }
        public ICollection<TagViewModel> Tags { get; set; }
    }
}
