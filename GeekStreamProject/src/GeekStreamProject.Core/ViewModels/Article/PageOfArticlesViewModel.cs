using System;
using System.Collections.Generic;

namespace GeekStreamProject.Core.ViewModels.Article
{
    public class PageOfArticlesViewModel
    {
        public IEnumerable<ArticleViewModel> Articles { get; set; }
    }
}
