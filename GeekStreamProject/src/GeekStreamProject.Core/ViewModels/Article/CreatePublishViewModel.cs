﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.ViewModels.Article
{
    public class CreatePublishViewModel
    {
        public int ArticleId { get; set; }

        public EArticleMode ArticleMode { get; set; } 
    }
}
