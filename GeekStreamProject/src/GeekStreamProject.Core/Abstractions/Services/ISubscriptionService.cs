﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.ViewModels.Subscription;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface ISubscriptionService
    {
        public Task SubscribeToAuthor(CreateSubscriptionViewModel model);

        public Task SubscribeToCategory(CreateSubscriptionViewModel model);

        public Task<IEnumerable<SubscriptionViewModel>> GetSubOnCategories(string subId);

        public Task<IEnumerable<SubscriptionViewModel>> SubscriptionsUser(string subId);

        public Task<IEnumerable<SubscriptionViewModel>> SubscribersUser(string subId);

        public Task Unsubscribe(int subId);
    }
}
