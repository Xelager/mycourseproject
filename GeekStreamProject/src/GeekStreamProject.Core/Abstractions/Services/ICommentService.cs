﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.ViewModels.Comment;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface ICommentService
    {
        public Task<IEnumerable<CommentViewModel>> GetAllComments(int articleId);

        public Task Create(CreateCommentViewModel comment);

        public Task DeleteComment(int id);

        public Task<IEnumerable<CommentViewModel>> GetReviewComments(int articleId);
    }
}
