﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.Review;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface IArticleReviewService
    {
        public Task SendReviewToModerators(int? articleId);

        public Task<IEnumerable<ReviewArticleViewModel>> GetAllReviewArticles();

        public Task<ReviewArticleViewModel> GetReviewArticle(int id);

        public Task ApproveArticle(CreateApproveViewModel model);

        public Task UnApprove(int articleReviewId);
        public Task<IEnumerable<ReviewArticleViewModel>> GetMyReviewArticles(string authorId);
    }
}
