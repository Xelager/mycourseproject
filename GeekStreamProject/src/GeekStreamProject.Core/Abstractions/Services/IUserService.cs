﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.User;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface IUserService
    {
        public Task<IEnumerable<AuthorViewModel>> GetAllUsers();

        public Task<UserViewModel> GetUser(string id);

        public Task<bool> IsUserNameExist(string userName);

        public Task<bool> IsEmailExist(string email);

        public Task<bool> Register(RegisterViewModel model, Func<string, string, string> confirmAccount);

        public Task<bool> ResetPassword(ForgotPasswordViewModel model, Func<string, string, string> resetAccount);

        public Task<bool> UpdatePassword(UpdatePasswordViewModel model);

        public Task UpdateAvatar(ChangeAvatarViewModel changeAvatarViewModel);
    }
}
