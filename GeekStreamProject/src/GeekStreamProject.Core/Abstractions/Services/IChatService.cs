﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.ViewModels.Chat;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface IChatService
    {
        public Task<IEnumerable<MessageViewModel>> GetLatestMessages();

        public Task Create(MessageViewModel model);
    }
}
