﻿using System;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface IEmailService
    {
        public Task SendEmailAsync(ApplicationUser user, string subject, Func<string, string, string> confirmAccount);

        public Task<bool> ConfirmAccount(string token, string userId);

        public Task SendResetPassword(ApplicationUser user, string subject, Func<string, string, string> resetAccount);
    }
}
