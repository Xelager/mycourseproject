﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.Review;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface IArticleService
    {
        public Task<IEnumerable<ArticleViewModel>> GetAllApprovedArticles();

        public Task<ArticleViewModel> GetArticle(int id);

        public Task<IEnumerable<ArticleViewModel>> GetArticlesByCategory(int categoryId);

        public Task<int> Create(CreateArticleViewModel model);

        public Task DeleteArticle(int id);

        public Task<IEnumerable<ArticleViewModel>> GetArticlesByAuthor(string authorId);

        public Task<ArticleViewModel> GetLastArticleAuthor(string authorId);

        public Task<ArticleViewModel> GetArticleByReview(int id);

        public Task<IEnumerable<ArticleViewModel>> GetMyDrafts(string authorId);

        public Task<IEnumerable<ArticleViewModel>> GetMyNotPublishedArticles(string authorId);

        public Task ToPublish(CreatePublishViewModel model);

        public Task ToDraft(int articleId);

        public Task ChangeRating(int id, int delta);
    }
}
