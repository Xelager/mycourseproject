﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.ViewModels.Category;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface ICategoryService
    {
        public Task<IEnumerable<CategoryViewModel>> GetAllCategories();

        public Task<CategoryViewModel> GetCategory(int id);

        public Task Create(CreateCategoryViewModel category);

        public Task DeleteCategory(int id);
    }
}
