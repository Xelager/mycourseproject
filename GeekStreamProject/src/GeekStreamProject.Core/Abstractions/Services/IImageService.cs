﻿using System.Threading.Tasks;
using GeekStreamProject.Core.ViewModels.Image;

namespace GeekStreamProject.Core.Abstractions.Services
{
    public interface IImageService
    {
        Task<int> CreateImage(ImageViewModel imageViewModel);

        public Task<ImageViewModel> InstallDefaultAvatar();
    }
}
