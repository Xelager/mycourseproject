﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using Microsoft.EntityFrameworkCore;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class ChatMessageRepository : Repository<ChatMessage>, IChatMessageRepository
    {
        private readonly ApplicationDbContext _context;

        public ChatMessageRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ChatMessage>> GetLatestChatMessages() =>
            await _context
                .ChatMessages
                .Include(message => message.Author)
                .Include(message => message.Author.Avatar)
                .OrderByDescending(message => message.SendedDate)
                .Take(50)
                .OrderBy(message => message.SendedDate)
                .ToListAsync();


    }
}
