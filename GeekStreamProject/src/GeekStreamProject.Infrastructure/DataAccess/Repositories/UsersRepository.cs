﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.ENums.ENums;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public UsersRepository(ApplicationDbContext context, RoleManager<ApplicationRole> roleManager)
        {
            _context = context;
            _roleManager = roleManager;
        }
        public async Task RegisterUser(ApplicationUser context)
        {
            await _context.Users.AddAsync(context);
            await _context.SaveChangesAsync();
        }

        public void LoginUser(ApplicationUser context)
        {

        }

        public async Task<ApplicationUser> SearchSameEmail(string Email)
        {
            return await _context.Users
                .Include(user => user.Avatar)
                .FirstOrDefaultAsync(user => user.Email == Email);
        }

        public async Task<ApplicationUser> GetUserProfile(string userName) =>
            await _context
                .Set<ApplicationUser>()
                .Include(user => user.Articles)
                .Include(user => user.Avatar)
                .FirstOrDefaultAsync(user => user.UserName == userName);

        public async Task<IEnumerable<ApplicationUser>> GetAllAuthors()
        {
            return await _context
                .Users
                .Include(user => user.Avatar)
                .Include(user => user.Articles)
                .OrderByDescending(user => user.Rating)
                .Where(x => x.Articles.Any(x => x.ArticleMode == EArticleMode.Published) && x.Articles.Where(x => x.ArticleMode == EArticleMode.Published).Count() > 0)
                .ToListAsync();
        }

        public async Task<bool> IsEmailExist(string email)
        {
            return await _context.Users
                .AnyAsync(x => x.NormalizedEmail == email.ToUpper());
        }

        public async Task<bool> IsUserNameExist(string userName)
        {
            return await _context.Users
                .AnyAsync(x => x.NormalizedUserName == userName.ToUpper());
        }

        public async Task Update(ApplicationUser user)
        {
            // assume Entity base class have an Id property for all items
            var entity = await _context.Users.FindAsync(user.Id);
            if (entity == null)
            {
                return;
            }

            _context.Entry(entity).CurrentValues.SetValues(user);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ApplicationUserRole>> GetUserInRole(string roleName)
        {
            var role = await _roleManager.Roles.SingleAsync(r => r.Name == roleName);
            return await _context
                .UserRoles
                .Where(x => x.RoleId == role.Id)
                .ToListAsync();
        }

    }
}
