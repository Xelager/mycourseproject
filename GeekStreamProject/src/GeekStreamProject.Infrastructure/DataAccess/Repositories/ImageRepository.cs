﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Configuration;
using GeekStreamProject.Core.Shared.Entities;
using SixLabors.ImageSharp;
using Image = GeekStreamProject.Core.Shared.Entities.Image;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class ImageRepository : Repository<Image>, IImageRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public ImageRepository(ApplicationDbContext context, IConfiguration configuration) : base(context)
        {
            _configuration = configuration;
            _context = context;
        }

        public async Task<int> SaveImage(Image imageDb, IFormFile formFile)
        {
            string uploadsFolder = Path.Combine(_configuration["ImageStorage"], imageDb.ImageType.ToString());
            string uniqueFileName = Guid.NewGuid().ToString() + "_" + formFile.FileName;
            string filePath = Path.Combine(uploadsFolder, uniqueFileName);
            using var imageLoad = SixLabors.ImageSharp.Image.Load(formFile.OpenReadStream());
            imageLoad.Save(filePath);

            imageDb.FilePath = Path.Combine("ImageStorage/", imageDb.ImageType.ToString() + "/", uniqueFileName);
            await Create(imageDb);
            return imageDb.Id;
        }

        public async Task<Image> GetDefaultAvatar() =>
            await _context
                .Set<Image>()
                .FirstOrDefaultAsync(image => image.ImageType == ENums.ENums.EImageTypes.DefaultAvatar);
    }
}

