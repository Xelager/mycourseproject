﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using Microsoft.EntityFrameworkCore.Query;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class ArticleReviewRepository : Repository<ArticleReview>, IArticleReviewRepository
    {
        private readonly ApplicationDbContext _context;

        public ArticleReviewRepository(ApplicationDbContext context) : base(context)
        {
            this._context = context;
        }

        public async Task<int> CreateReview(ArticleReview review)
        {
            await Create(review);
            return review.Id;
        }
        
        public async Task DeleteCascade(int? reviewId)
        {
            if (reviewId is null) return;
            var review = await _context
                .Set<ArticleReview>()
                .Include(review => review.ReviewComments)
                .Include(review => review.Approves)
                .Include(review => review.Reviewers)
                .FirstOrDefaultAsync(review => review.Id == reviewId);
            _context.Remove(review);
            _context.SaveChanges();
        }

    }
}

