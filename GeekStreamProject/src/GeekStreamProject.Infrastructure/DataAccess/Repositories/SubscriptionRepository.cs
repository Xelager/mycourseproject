﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class SubscriptionRepository : Repository<Subscription>, ISubscriptionRepository
    {
        private readonly ApplicationDbContext _context;

        public SubscriptionRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Subscription>> SubscriptionsUser(string subId) => //Подписки
            await _context.Subscriptions
            .Where(sub => sub.SubscriberId == subId)
            .Include(sub => sub.Author)
            .Include(sub => sub.Author.Avatar)
            .Include(sub => sub.Author.Articles)
            .Include(sub => sub.Subscriber)
            .Include(sub => sub.Subscriber.Avatar)
            .Include(sub => sub.Subscriber.Articles)
            .OrderByDescending(sub => sub.Author.Rating)
            .ToListAsync();

        public async Task<IEnumerable<Subscription>> SubscribersUser(string subId) => //Подписчики
            await _context.Subscriptions
                .Where(sub => sub.AuthorId == subId)
                .Include(sub => sub.Subscriber)
                .Include(sub => sub.Subscriber.Avatar)
                .Include(sub => sub.Subscriber.Articles)
                .Include(sub => sub.Author)
                .Include(sub => sub.Author.Avatar)
                .Include(sub => sub.Author.Articles)
                .OrderByDescending(sub => sub.Subscriber.Rating)
                .ToListAsync();

        public async Task<IEnumerable<Subscription>> SubOnCategories(string subId) =>
            await _context.Subscriptions
            .Where(sub => sub.SubscriberId == subId)
            .Include(sub => sub.Category)
            .Include(sub => sub.Category.Image)
            .Include(sub => sub.Category.Cover)
            .ToListAsync();
    }
}

