﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using Microsoft.EntityFrameworkCore.Query;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class ArticleReviewerUserRepository : Repository<ArticleReviewerUser>, IArticleReviewerUserRepository
    {
        private readonly ApplicationDbContext _context;

        public ArticleReviewerUserRepository(ApplicationDbContext context) : base(context)
        {
            this._context = context;
        }

    }
}

