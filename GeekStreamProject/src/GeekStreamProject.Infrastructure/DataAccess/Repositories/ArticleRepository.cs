﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GeekStreamProject.ENums.ENums;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using Microsoft.EntityFrameworkCore.Query;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class ArticleRepository : Repository<Article>, IArticleRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IArticleReviewRepository _articleReviewRepository;

        public ArticleRepository(ApplicationDbContext context, IArticleReviewRepository articleReviewRepository) : base(context)
        {
            this._context = context;
            _articleReviewRepository = articleReviewRepository;
        }

        public async Task<IEnumerable<Article>> GetArticlesByCategory(int categoryId)
        {
            return await _context.Articles
                .Where(article => article.CategoryId == categoryId && article.ArticleMode == EArticleMode.Published)
                .Include(article => article.Category)
                .Include(article => article.Category.Cover)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .ToListAsync();
        }

        public async Task<Article> GetArticleByReview(int reviewId)
        {
            return await _context.Set<Article>()
                .Include(article => article.Category)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .FirstOrDefaultAsync(article => article.ArticleReview.Id == reviewId);
        }

        public async Task<IEnumerable<Article>> GetPublishedArticles() =>
            await _context.Articles
                .Where(article => article.ArticleMode == EArticleMode.Published)
                .Include(article => article.Category)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .ToListAsync();

        public async Task<IEnumerable<Article>> GetDraftArticles(string authorId) =>
            await _context.Articles
                .Where(article => article.ArticleMode == EArticleMode.Draft && article.AuthorId == authorId)
                .Include(article => article.Category)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .ToListAsync();

        public async Task<IEnumerable<Article>> GetNotPublishedArticles(string authorId) =>
            await _context.Set<Article>()
                .Include(article => article.Category)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .Where(article => article.ArticleMode == EArticleMode.InProcessPublished && article.AuthorId == authorId)
                .ToListAsync();

        public async Task<IEnumerable<Article>> GetAllArticlesOnReview() =>
            await _context.Articles
                .Include(article => article.Category)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .Where(article => article.ArticleMode == EArticleMode.InProcessPublished || article.ArticleMode == EArticleMode.UnderReview)
                .ToListAsync();

        public async Task<IEnumerable<Article>> GetMyArticlesOnReview(string authorId) =>
            await _context.Articles
                .Include(article => article.Category)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .Where(article => (article.AuthorId == authorId) && (article.ArticleMode == EArticleMode.InProcessPublished || article.ArticleMode == EArticleMode.UnderReview))
                .ToListAsync();

        public async Task<Article> GetOneArticle(int id) =>
            await _context
                .Set<Article>()
                .Include(article => article.Category)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .FirstOrDefaultAsync(article => article.Id == id);


        public async Task<IEnumerable<Article>> GetArticlesByAuthor(string authorId) =>
            await _context.Articles
            .Where(article => article.AuthorId == authorId && article.ArticleMode == EArticleMode.Published)
            .Include(article => article.Category)
            .Include(article => article.Category.Image)
            .Include(article => article.Author)
            .Include(article => article.Author.Avatar)
            .Include(article => article.ArticleReview)
            .Include(article => article.ArticleReview.ReviewComments)
            .Include(article => article.ArticleReview.Approves)
            .Include(article => article.Comments)
            .Include(article => article.Image)
            .ToListAsync();

        public async Task DeleteArticleCascade(int articleId)
        {
            var article = await _context
                .Set<Article>()
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.ArticleReview.Reviewers)
                .FirstOrDefaultAsync(article => article.Id == articleId);
            _context.Remove(article);
            _context.SaveChanges();
        }
        public async Task<Article> GetLastArticleAuthor(string authorId) =>
        await _context.Articles
                .Where(article => article.AuthorId == authorId && article.ArticleMode == EArticleMode.Published)
                .Include(article => article.Category)
                .Include(article => article.Category.Image)
                .Include(article => article.Author)
                .Include(article => article.Author.Avatar)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .OrderByDescending(article => article.Id)
                .FirstOrDefaultAsync();

        public async Task AddToDraft(int articleId)
        {
            await PrepareForDraft(articleId);
        }

        private async Task PrepareForDraft(int articleId)
        {
            var article = await _context
                .Set<Article>()
                .Include(article => article.Comments)
                .Include(article => article.ArticleReview.ReviewComments)
                .Include(article => article.ArticleReview)
                .Include(article => article.ArticleReview.Approves)
                .Include(article => article.ArticleReview.Reviewers)
                .FirstOrDefaultAsync(article => article.Id == articleId);
            article.Rating = 0;
            article.Views = 0;
            var CloneArticle = new Article()
            {
                Title = article.Title,
                Content = article.Content,
                Description = article.Description,
                Views = 0,
                Rating = 0,
                ArticleMode = EArticleMode.Draft,
                DateOfCreation = article.DateOfCreation,
                AuthorId = article.AuthorId,
                CategoryId = article.CategoryId,
                ImageId = article.ImageId
            };

            await _articleReviewRepository.DeleteCascade(article.ArticleReviewId);
            _context.Remove(article);
            await CreateArticle(CloneArticle);
            _context.SaveChanges();

        }

        public async Task<int> CreateArticle(Article article)
        {
            await Create(article);
            return article.Id;
        }

        public async Task Update(Article article)
        {
            // assume Entity base class have an Id property for all items
            var entity = await _context.Articles.FindAsync(article.Id);
            if (entity == null)
            {
                return;
            }

            _context.Entry(entity).CurrentValues.SetValues(article);
            await _context.SaveChangesAsync();
        }

        public async Task ChangeRatingArticle(int id, int delta)
        {
            var article = _context.Articles.FirstOrDefault(article => article.Id == id);

            article.Rating += delta;
            await Update(article);
            _context
                .SaveChanges();
        }

        

    }
}

