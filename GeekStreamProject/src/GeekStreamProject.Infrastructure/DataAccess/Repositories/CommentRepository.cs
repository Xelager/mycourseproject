﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        private readonly ApplicationDbContext _context;

        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Comment>> GetComments(int articleId) =>
            await _context
                .Set<Comment>()
                .Include(Comment => Comment.Article)
                .Include(Comment => Comment.ArticleReview)
                .Include(Comment => Comment.Commentator.Avatar)
                .Include(Comment => Comment.Commentator)
                .Include(comment => comment.ParentComment)
                .Where(x => x.ArticleId.HasValue && x.ArticleId == articleId && !x.ArticleReviewId.HasValue)
                .ToListAsync();

        public async Task DeleteCommentCascade(int parrentCommentId)
        {
            var comment = await _context
                .Set<Comment>()
                .Include(comment => comment.ChildComments)
                .FirstOrDefaultAsync(comment => comment.Id == parrentCommentId);
            _context.Remove(comment);
            _context.SaveChanges();
        }

        public async Task<Comment> GetComment(int? commentId) =>
            await _context
                .Set<Comment>()
                .Include(Comment => Comment.Article)
                .Include(Comment => Comment.ArticleReview)
                .Include(Comment => Comment.Commentator.Avatar)
                .Include(Comment => Comment.Commentator)
                .Include(comment => comment.ParentComment)
                .Where(x => x.ArticleId.HasValue)
                .FirstOrDefaultAsync(comment => comment.Id == commentId);

        public async Task<IEnumerable<Comment>> GetReviewComments(int articleId) =>
            await _context
                .Set<Comment>()
                .Include(Comment => Comment.Article)
                .Include(Comment => Comment.ArticleReview)
                .Include(Comment => Comment.ArticleReview.ReviewComments)
                .Include(Comment => Comment.Commentator.Avatar)
                .Include(Comment => Comment.Commentator)
                .Include(comment => comment.ParentComment)
                .Where(x => x.ArticleId.HasValue && x.ArticleId == articleId && x.ArticleReviewId.HasValue)
                .ToListAsync();

    }
}

