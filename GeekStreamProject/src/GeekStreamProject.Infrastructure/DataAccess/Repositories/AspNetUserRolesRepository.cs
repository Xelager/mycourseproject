﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class AspNetUserRolesRepository
    {
        private readonly ApplicationDbContext _context;


        public AspNetUserRolesRepository(ApplicationDbContext context)
        {
            _context = context;
           
        }

        public async Task<IEnumerable<ApplicationUserRole>> GetUsers(string roleName)
        {
            return await _context.UserRoles.Include(x => x.User).ToListAsync();
        }
       

    }
}
