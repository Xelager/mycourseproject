﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        private readonly ApplicationDbContext _context;

        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Category>> GetCategories() =>
            await _context
                .Set<Category>()
                .Include(category => category.Cover)
                .Include(category => category.Image)
                .Include(category => category.Articles)
                .Include(category => category.Subscribers)
                .ToListAsync();

        public async Task<Category> GetOneCategory(int id) =>
            await _context
                .Set<Category>()
                .Include(category => category.Image)
                .Include(category => category.Cover)
                .Include(category => category.Articles)
                .Include(category => category.Subscribers)
                .FirstOrDefaultAsync(category => category.Id == id);

        public async Task DeleteCategoryCascade(int categoryId)
        {
            var category = await _context
                .Set<Category>()
                .Include(category => category.Cover)
                .Include(category => category.Image)
                .FirstOrDefaultAsync(category => category.Id == categoryId);
            _context.Remove(category);
            _context.SaveChanges();
        }
    }
}

