﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Query;

namespace GeekStreamProject.Infrastructure.DataAccess.Repositories
{
    public class ApproveRepository : Repository<Approve>, IApproveRepository
    {
        private readonly ApplicationDbContext _context;

        public ApproveRepository(ApplicationDbContext context) : base(context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Approve>> GetApproves(int articleReviewId)
        {
            return await _context
                .Set<Approve>()
                .Where(x => x.ArticleReviewId == articleReviewId)
                .Include(x => x.User)
                .Include(x => x.ArticleReview)
                .ToListAsync();
        }

    }
}

