﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string,
        IdentityUserClaim<string>, ApplicationUserRole, IdentityUserLogin<string>, IdentityRoleClaim<string>,
        IdentityUserToken<string>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ApplicationRoleMap());
            builder.ApplyConfiguration(new ApplicationUserRoleMap());
            builder.ApplyConfiguration(new ApplicationUserMap());
            builder.ApplyConfiguration(new ArticleMap());
            builder.ApplyConfiguration(new ArticleReviewMap());
            builder.ApplyConfiguration(new CategoryMap());
            builder.ApplyConfiguration(new CommentMap());
            builder.ApplyConfiguration(new ImageMap());
            builder.ApplyConfiguration(new SubscriptionMap());
            builder.ApplyConfiguration(new TagMap());
            builder.ApplyConfiguration(new ChatMap());
            builder.ApplyConfiguration(new ArticleReviewerUserMap());

            base.OnModelCreating(builder);
        }

        public DbSet<Article> Articles { get; set; }

        public DbSet<ArticleReviewerUser> ArticleReviewerUsers { get; set; }

        public DbSet<Approve> Approves { get; set; }

        public DbSet<ArticleReview> ArticleReviews { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Image> Images { get; set; }

        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<Chat> Chats { get; set; }

        public DbSet<ChatMessage> ChatMessages { get; set; }
    }
}
