﻿using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap
{
    public class ArticleReviewerUserMap : IEntityTypeConfiguration<ArticleReviewerUser>
    {
        public void Configure(EntityTypeBuilder<ArticleReviewerUser> builder)
        {
            builder.HasKey(x => new { x.UserId, x.ArticleReviewId });

            builder
                .HasOne(x => x.User)
                .WithMany(x => x.Reviews)
                .HasForeignKey(x => x.UserId);

            builder
                .HasOne(x => x.ArticleReview)
                .WithMany(x => x.Reviewers)
                .HasForeignKey(x => x.ArticleReviewId);
        }
    }
}

