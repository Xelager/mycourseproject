﻿using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SixLabors.ImageSharp.ColorSpaces;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap
{
    class ChatMap : IEntityTypeConfiguration<Chat>
    {
        public void Configure(EntityTypeBuilder<Chat> builder)
        {
            builder
                .ToTable("Chat");

            builder
                .HasMany(x => x.ChatMessages)
                .WithOne(x => x.Chat)
                .HasForeignKey(x => x.ChatId);

            builder
                .HasMany(x => x.ChatUsers)
                .WithMany(x => x.Chats)
                .UsingEntity(x => x.ToTable("ChatUser"));
        }

    }
}
