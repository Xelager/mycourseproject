﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap
{
    public class SubscriptionMap : IEntityTypeConfiguration<Subscription>
    {
        public void Configure(EntityTypeBuilder<Subscription> builder)
        {
            builder
                .ToTable("Subscriptions");

            builder
                .HasOne(x => x.Author)
                .WithMany(x => x.Subscribers)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.AuthorId);

            builder
                .HasOne(x => x.Subscriber)
                .WithMany(x => x.Subscriptions)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.SubscriberId);

            builder
                .HasOne(x => x.Category)
                .WithMany(x => x.Subscribers)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.CategoryId);
        }
    }
}

