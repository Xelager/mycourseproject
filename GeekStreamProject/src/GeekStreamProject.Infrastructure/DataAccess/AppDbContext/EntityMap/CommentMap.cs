﻿using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap
{
    public class CommentMap : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder
                .ToTable("Comments");
        }
    }
}

