﻿using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap
{
    public class TagMap : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder
                .ToTable("Tags");
        }
    }
}

