﻿using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap
{
    public class ApplicationUserMap : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder
                .ToTable("ApplicationUsers");

            builder.Property(x => x.FirstName)
                .HasColumnType("nvarchar(150)")
                .IsRequired();
            
            builder.Property(x => x.LastName)
                .HasColumnType("nvarchar(150)")
                .IsRequired();

            builder
                .HasMany(x => x.Articles)
                .WithOne(x => x.Author)
                .HasForeignKey(x => x.AuthorId);

            builder
                .HasOne(x => x.Avatar)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.AvatarId);

            builder
                .HasMany(x => x.ApplicationUserRoles)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);

            builder
                .HasMany(x => x.ListOfComments)
                .WithOne(x => x.Commentator)
                .HasForeignKey(x => x.CommentatorId);
        }
    }
}
