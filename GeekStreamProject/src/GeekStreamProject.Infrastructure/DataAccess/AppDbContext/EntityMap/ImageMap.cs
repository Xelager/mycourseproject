﻿using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap
{
    public class ImageMap : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
            builder
                .ToTable("Images");

            builder
                .HasOne(x => x.Category)
                .WithOne(x => x.Image)
                .HasForeignKey<Category>(x => x.ImageId);

            builder
                .HasMany(x => x.Users)
                .WithOne(x => x.Avatar)
                .HasForeignKey(x => x.AvatarId);

            builder
                .HasOne(x => x.Article)
                .WithOne(x => x.Image)
                .HasForeignKey<Article>(x => x.ImageId);
        }
    }
}

