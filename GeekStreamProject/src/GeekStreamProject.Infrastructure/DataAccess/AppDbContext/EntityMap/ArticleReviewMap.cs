﻿using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext.EntityMap
{
    public class ArticleReviewMap : IEntityTypeConfiguration<ArticleReview>
    {
        public void Configure(EntityTypeBuilder<ArticleReview> builder)
        {
            builder
                .ToTable("ArticleReviews");

            builder
                .HasOne(x => x.Article)
                .WithOne(x => x.ArticleReview)
                .HasForeignKey<ArticleReview>(x => x.ArticleId);

            builder
                .HasMany(x => x.ReviewComments)
                .WithOne(x => x.ArticleReview)
                .HasForeignKey(x => x.ArticleReviewId);
        }
    }
}

