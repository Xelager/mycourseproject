﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.Image;
using GeekStreamProject.ENums.ENums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext
{
    public class UserSeed
    {
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly IConfiguration _configuration;

        private readonly RoleManager<ApplicationRole> _roleManager;


        public UserSeed(UserManager<ApplicationUser> userManager, IConfiguration configuration,
            RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _configuration = configuration;
            _roleManager = roleManager;
        }

        public async Task Seed()
        {
            await SeedUserRoles();
            await SeedAdmin();
            await SeedModerator();
        }

        private async Task SeedUserRoles()
        {
            if (!await _roleManager.RoleExistsAsync("Admin") && !await _roleManager.RoleExistsAsync("User") &&
                !await _roleManager.RoleExistsAsync("Moderator"))
            {
                await _roleManager.CreateAsync(new ApplicationRole("Moderator"));
                await _roleManager.CreateAsync(new ApplicationRole("User"));
                await _roleManager.CreateAsync(new ApplicationRole("Admin"));
            }
        }

        private async Task SeedAdmin()
        {
            if (await _userManager.FindByEmailAsync(_configuration["Admin:Email"]) == null)
            {
                var admin = new ApplicationUser()
                {
                    Email = _configuration["Admin:Email"],
                    FirstName = _configuration["Admin:FirstName"],
                    LastName = _configuration["Admin:LastName"],
                    UserName = _configuration["Admin:UserName"],
                    EmailConfirmed = true,
                    AvatarId = null
                };

                await _userManager.CreateAsync(admin, _configuration["Admin:Password"]);
                await _userManager.AddToRoleAsync(admin, "Admin");
            }
        }

        private async Task SeedModerator()
        {
            if (await _userManager.FindByEmailAsync(_configuration["Moderator:Email"]) == null)
            {

                var admin = new ApplicationUser()
                {
                    Email = _configuration["Moderator:Email"],
                    FirstName = _configuration["Moderator:FirstName"],
                    LastName = _configuration["Moderator:LastName"],
                    UserName = _configuration["Moderator:UserName"],
                    EmailConfirmed = true,
                    AvatarId = null
                };

                await _userManager.CreateAsync(admin, _configuration["Moderator:Password"]);
                await _userManager.AddToRoleAsync(admin, "User");
                await _userManager.AddToRoleAsync(admin, "Moderator");
            }
        }
    }
}
