﻿using System.Collections.Generic;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace GeekStreamProject.Infrastructure.DataAccess.AppDbContext
{
    public static class ModelBuilderExtensions
    {
        public static void SeedTags(this ModelBuilder modelBuilder)
        {
            var data = new List<Tag>()
            {
                new() {Id = 1, WorldOfTag = "Title 1"},
                new() {Id = 2, WorldOfTag = "Title 2"},
                new() {Id = 3, WorldOfTag = "Title 3"},
                new() {Id = 4, WorldOfTag = "Title 4"},
            };

            modelBuilder.Entity<Tag>()
                .HasData(data);
        }

        public static void SeedCommentaries(this ModelBuilder modelBuilder)
        {
            var data = new List<Comment>()
            {
                new() {Id = 1, ArticleId = 1, Content = "Article 1 comment body"},
                new() {Id = 2, ArticleId = 2, Content = "Article 2 comment body"},
                new() {Id = 3, ArticleId = 3, Content = "Article 3 comment body"},
                new() {Id = 4, ArticleId = 4, Content = "Article 4 comment body"},
            };

            modelBuilder.Entity<Comment>()
                .HasData(data);
        }

        public static void SeedArticle(this ModelBuilder modelBuilder)
        {
            var data = new List<Article>()
            {
                new()
                {
                    Id = 20,
                    Title = "Title 1",
                    Content = "Body 1",
                    CategoryId = null,
                    Rating = 100,
                    AuthorId = null,
                    Views = 1000,
                    ArticleReviewId = null,
                },
                new()
                {
                    Id = 21,
                    Title = "Title 2",
                    Content = "Body 2",
                    Rating = 100,
                    Views = 1000,
                },
                new()
                {
                    Id = 22,
                    Title = "Title 3",
                    Content = "Body 3",
                    CategoryId = null,
                    Rating = 100,
                    AuthorId = null,
                    Views = 1000,
                    ArticleReviewId = null,
                },
                new()
                {
                    Id = 23,
                    Title = "Title 4",
                    Content = "Body 4",
                    CategoryId = null,
                    Rating = 100,
                    AuthorId = null,
                    Views = 1000,
                    ArticleReviewId = null,
                }
            };

            modelBuilder.Entity<Article>()
                .HasData(data);
        }
        public static void SeedCategory(this ModelBuilder modelBuilder)
        {
            var data = new List<Category>()
            {
                new() {Id = 1, Name = "Наука"}, new() {Id = 2, Name = "Искусство"}, new() {Id = 3, Name = "Готовка"},
            };

            modelBuilder.Entity<Category>()
                .HasData(data);
        }
    }
}
