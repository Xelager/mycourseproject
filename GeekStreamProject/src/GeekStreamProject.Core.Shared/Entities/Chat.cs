﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Chat : BaseId
    {
        public ICollection<ChatMessage> ChatMessages { get; set; }

        public ICollection<ApplicationUser> ChatUsers { get; set; }
    }
}
