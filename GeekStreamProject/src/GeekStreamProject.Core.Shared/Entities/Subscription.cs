﻿namespace GeekStreamProject.Core.Shared.Entities
{
    public class Subscription : BaseId
    {
        public string SubscriberId { get; set; }

        public virtual ApplicationUser Subscriber { get; set; }

        public string AuthorId { get; set; }

        public virtual ApplicationUser Author { get; set; }

        public int? CategoryId { get; set; }

        public virtual Category Category { get; set; }
    }
}
