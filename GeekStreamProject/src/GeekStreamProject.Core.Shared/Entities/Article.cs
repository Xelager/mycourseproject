using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Article : BaseId
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public string Description { get; set; }

        public int? Views { get; set; }

        public int? Rating { get; set; }

        public DateTime DateOfCreation { get; set; }

        public int? ArticleReviewId { get; set; }

        public virtual ArticleReview ArticleReview { get; set; }

        public int? CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public string AuthorId { get; set; }

        public virtual ApplicationUser Author { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }

        public EArticleMode ArticleMode { get; set; }
    }
}
