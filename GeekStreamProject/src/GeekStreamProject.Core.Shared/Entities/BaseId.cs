using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class BaseId
    {
        public int Id { get; set; }
    }
}
