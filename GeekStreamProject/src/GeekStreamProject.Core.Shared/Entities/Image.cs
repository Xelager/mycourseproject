using System;
using System.Collections;
using System.Collections.Generic;
using GeekStreamProject.ENums;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Image : BaseId
    {
        public string FilePath { get; set; }

        public EImageTypes ImageType { get; set; }

        public virtual Article Article { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}
