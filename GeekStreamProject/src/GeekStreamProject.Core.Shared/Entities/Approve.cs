﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Approve : BaseId
    {
        public bool IsApproved { get; set; }

        public int ArticleReviewId { get; set; }

        public virtual ArticleReview ArticleReview { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }
    }

}
