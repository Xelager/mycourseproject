using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Category : BaseId
    {
        public string Name { get; set; }

        public int? CoverId { get; set; }

        public Image Cover { get; set; }

        public int? ImageId { get; set; }

        public Image Image { get; set; }

        public virtual ICollection<Article> Articles { get; set; }

        public virtual ICollection<Subscription> Subscribers { get; set; }
    }
}
