﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using GeekStreamProject.ENums;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class ArticleReviewerUser
    {
        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public int? ArticleReviewId { get; set; }

        public virtual ArticleReview ArticleReview { get; set; }

    }
}
