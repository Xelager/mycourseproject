using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class ArticleReview : BaseId
    {
        public virtual ICollection<ArticleReviewerUser> Reviewers { get; set; }

        public int? ArticleId { get; set; }

        public virtual Article Article { get; set; }

        public virtual ICollection<Comment> ReviewComments { get; set; }

        public virtual ICollection<Approve> Approves { get; set; }

    }
}
