﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using GeekStreamProject.ENums;
using GeekStreamProject.ENums.ENums;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfCreation { get; set; }
        public int? Rating { get; set; }
        public int? AvatarId { get; set; }
        public virtual Image Avatar { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Subscription> Subscribers { get; set; }
        public virtual ICollection<Subscription> Subscriptions { get; set; }
        public virtual ICollection<ApplicationUserRole> ApplicationUserRoles { get; set; }
        public virtual ICollection<Comment> ListOfComments { get; set; }
        public virtual ICollection<Chat> Chats { get; set; }

        public virtual ICollection<ArticleReviewerUser> Reviews { get; set; }
    }
}
