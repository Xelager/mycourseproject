﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class ChatMessage : BaseId
    {
        public string AuthorId { get; set; }

        public virtual ApplicationUser Author { get; set; }

        public DateTime SendedDate { get; set; } = DateTime.UtcNow;

        public string Content { get; set; }

        public int? ChatId { get; set; }

        public Chat Chat { get; set; }
    }
}
