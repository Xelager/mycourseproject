using System;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Comment : BaseId
    {
        public string Content { get; set; }

        public int? Rating { get; set; }

        public DateTime DateOfCreation { get; set; }

        public int? ParentCommentId { get; set; }

        public virtual Comment ParentComment { get; set; }

        public string CommentatorId { get; set; }

        public virtual ApplicationUser Commentator { get; set; }
        
        public int? ArticleId { get; set; }

        public virtual Article Article { get; set; }

        public int? ArticleReviewId { get; set; }

        public virtual ArticleReview ArticleReview { get; set; }

        public virtual ICollection<Comment> ChildComments { get; set; }
    }
}
