﻿using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Tag : BaseId
    {
        public string WorldOfTag { get; set; } 

        public virtual ICollection<Article> Articles { get; set; }
    }
}
