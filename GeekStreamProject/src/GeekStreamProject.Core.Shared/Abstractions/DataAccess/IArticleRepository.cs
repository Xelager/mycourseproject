﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IArticleRepository : IRepository<Article>
    {
        public  Task<IEnumerable<Article>> GetArticlesByCategory(int categoryId);
        public Task<IEnumerable<Article>> GetPublishedArticles();
        public Task<IEnumerable<Article>> GetDraftArticles(string authorId);
        public Task<IEnumerable<Article>> GetNotPublishedArticles(string authorId);
        public Task<IEnumerable<Article>> GetAllArticlesOnReview();
        public Task<IEnumerable<Article>> GetMyArticlesOnReview(string authorId);
        public Task<Article> GetOneArticle(int id);
        public Task<IEnumerable<Article>> GetArticlesByAuthor(string authorId);
        public Task DeleteArticleCascade(int articleId);
        public Task<Article> GetLastArticleAuthor(string authorId);
        public  Task<int> CreateArticle(Article article);
        public Task<Article> GetArticleByReview(int reviewId);
        public Task Update(Article article);

        public Task AddToDraft(int articleId);

        public Task ChangeRatingArticle(int id, int delta);
    }
}
