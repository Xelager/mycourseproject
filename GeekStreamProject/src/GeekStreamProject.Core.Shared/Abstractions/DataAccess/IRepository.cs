﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        public Task Create(TEntity entity);
        public Task Delete(object id);
        public Task Save();
    }
}
