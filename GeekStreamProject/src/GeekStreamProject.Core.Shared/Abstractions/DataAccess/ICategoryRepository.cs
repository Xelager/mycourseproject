﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ICategoryRepository : IRepository<Category>
    {
        public Task<IEnumerable<Category>> GetCategories();
        public Task<Category> GetOneCategory(int id);

        public Task DeleteCategoryCascade(int categoryId);
    }
}
