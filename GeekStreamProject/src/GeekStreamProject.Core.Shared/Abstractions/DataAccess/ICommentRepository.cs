﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ICommentRepository : IRepository<Comment>
    {
        public Task<IEnumerable<Comment>> GetComments(int articleId);

        public Task<Comment> GetComment(int? commentId);

        public Task DeleteCommentCascade(int parrentCommentId);

        public Task<IEnumerable<Comment>> GetReviewComments(int articleId);
    }
}
