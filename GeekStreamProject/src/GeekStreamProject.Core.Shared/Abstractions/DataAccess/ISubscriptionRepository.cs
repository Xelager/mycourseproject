﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ISubscriptionRepository : IRepository<Subscription>
    {
        public Task<IEnumerable<Subscription>> SubscriptionsUser(string subId);

        public Task<IEnumerable<Subscription>> SubscribersUser(string subId);
        public Task<IEnumerable<Subscription>> SubOnCategories(string subId);
    }
}
