﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IChatRepository : IRepository<Chat>
    {
    }
}
