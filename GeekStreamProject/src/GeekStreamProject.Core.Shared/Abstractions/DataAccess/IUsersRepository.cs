﻿using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IUsersRepository
    {
        public Task RegisterUser(ApplicationUser model);
        
        public void LoginUser(ApplicationUser model);

        public Task<ApplicationUser> SearchSameEmail(string Email);

        Task<ApplicationUser> GetUserProfile(string id);

        public Task<IEnumerable<ApplicationUser>> GetAllAuthors();

        public Task<bool> IsUserNameExist(string userName);

        public Task<bool> IsEmailExist(string email);

        public Task Update(ApplicationUser user);
        public Task<IEnumerable<ApplicationUserRole>> GetUserInRole(string roleName);
    }
}
