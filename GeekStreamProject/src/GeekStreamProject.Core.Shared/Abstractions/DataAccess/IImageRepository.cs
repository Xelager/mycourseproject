﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IImageRepository
    {
        public Task<int> SaveImage(Image image, IFormFile formFile);

        public Task<Image> GetDefaultAvatar();
    }
}
