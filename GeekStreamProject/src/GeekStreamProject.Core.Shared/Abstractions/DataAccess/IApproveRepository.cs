﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IApproveRepository : IRepository<Approve>
    {
        public Task<IEnumerable<Approve>> GetApproves(int articleReviewId);
    }
}
