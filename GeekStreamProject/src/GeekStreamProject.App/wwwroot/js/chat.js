﻿"use strict";

(async function () {
    var connection = new signalR.HubConnectionBuilder().withUrl("/chat").build();

    //Disable send button until connection is established
    document.getElementById("sendButton").disabled = true;

    connection.on("ReceiveMessage", (user, message, date) => {
        const a = document.createElement("a");
        a.classList.add("list-group-item", "list-group-item-action", "flex-column", "align-items-start");

        const div = document.createElement("div");
        div.classList.add("d-flex", "w-100", "justify-content-between");

        const h5 = document.createElement("h5");
        h5.classList.add("mb-1");
        h5.textContent = `${user}`;

        const small = document.createElement("small");
        small.classList.add("text-muted");
        small.textContent = `${date}`;

        const p = document.createElement("p");
        small.classList.add("mb-1");
        p.textContent = `${message}`;

         
        div.appendChild(h5);
        div.appendChild(small);

        a.appendChild(div);
        a.appendChild(p);

        document.getElementById("messagesList").appendChild(a);
         
        });

    connection.start().then(function () {
        document.getElementById("sendButton").disabled = false;
    }).catch(function (err) {
        return console.error(err.toString());
    });

    document.getElementById("sendButton").addEventListener("click", function (event) {
        const user = document.getElementById("userInput").value;
        const message = document.getElementById("messageInput").value;
        connection.invoke("SendMessage",message).catch(function (err) {
            return console.error(err.toString());
        });
        event.preventDefault();
        document.getElementById("messageInput").value = "";
    });

})();


