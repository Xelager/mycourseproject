using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.User;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;


namespace GeekStreamProject.App.Models
{
    public class AddAvatarClaimsTransformation : IClaimsTransformation
    {
        private readonly IUsersRepository _userRepository;

        public AddAvatarClaimsTransformation(IUsersRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            // Clone current identity
            var clone = principal.Clone();
            var newIdentity = (ClaimsIdentity)clone.Identity;

            // Support AD and local accounts
            var nameId = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
            if (nameId == null)
            {
                return principal;
            }

            // Get user from database
            var user = await _userRepository.GetUserProfile(nameId.Value);
            if (user is null || user.Avatar is null)
            {
                return principal;
            }

            // Add role claims to cloned identity
            var claim = new Claim("avatarUrl", user.Avatar.FilePath);
            newIdentity.AddClaim(claim);

            return clone;
        }
    }


}
