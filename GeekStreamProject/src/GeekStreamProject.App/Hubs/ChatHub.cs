﻿using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.ViewModels.Chat;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions.Services;

namespace GeekStreamProject.App.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private IChatService _chatService;
        public ChatHub(IChatService chatService, IHttpContextAccessor httpContextAccessor)
        {
            _chatService = chatService;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task SendMessage(string message)
        {
            var userName = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            await _chatService.Create(new MessageViewModel 
            {
                Content = message,
                AuthorName = userName,
            });
            await Clients.All.SendAsync("ReceiveMessage", userName, message, DateTime.UtcNow.ToString());
        }

        public async Task SendComment(string message)
        {
            var userName = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            await _chatService.Create(new MessageViewModel
            {
                Content = message,
                AuthorName = userName,
            });
            await Clients.All.SendAsync("ReceiveMessage", userName, message, DateTime.UtcNow.ToString());
        }
    }
}
