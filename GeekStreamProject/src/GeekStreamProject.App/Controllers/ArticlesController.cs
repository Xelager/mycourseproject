﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.ENums.ENums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace GeekStreamProject.App.Controllers
{
    public class ArticlesController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ICategoryService _categoryService;
        private readonly ICommentService _commentService;
        private readonly IArticleReviewService _articleReviewService;

        public ArticlesController(IArticleService articleService, ICategoryService categoryService,
            UserManager<ApplicationUser> userManager, ICommentService commentService, IArticleReviewService articleReviewService)
        {
            _articleService = articleService;
            _categoryService = categoryService;
            _userManager = userManager;
            _commentService = commentService;
            _articleReviewService = articleReviewService;
        }

        // GET: Articles
        [HttpGet]
        public async Task<IActionResult> Articles()
        {
            var articleViewModels = await _articleService.GetAllApprovedArticles();

            if (articleViewModels == null)
            {
                return NotFound();
            }

            return View(articleViewModels);
        }

        // GET: Articles/Details/5
        [HttpGet]
        public async Task<IActionResult> DetailArticle(int id)
        {
            var articleViewModels = await _articleService.GetArticle(id);
            if (articleViewModels == null)
            {
                return NotFound();
            }
            ViewBag.Id = id;
            return View(articleViewModels);
        }

        [HttpGet]
        public IActionResult AddNewComment()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            return RedirectToAction("Articles", "Articles");
        }

        [HttpGet]
        public async Task<IActionResult> ChangeRating(int id, int delta)
        {
            await _articleService.ChangeRating(id, delta);
            return RedirectToAction("Articles");
        }

        [HttpPost]
        public async Task<IActionResult> AddNewComment(CreateCommentViewModel createCommentViewModel, bool articleReview = false)
        {
            if (!ModelState.IsValid)
            {
                if (articleReview)
                {
                    return RedirectToAction("ReviewArticle", "ArticleReviews", new { id = createCommentViewModel.ArticleId });
                }
                return RedirectToAction("DetailArticle", new { Id = createCommentViewModel.ArticleId });
            }
            await _commentService.Create(createCommentViewModel);
            if (articleReview)
            {
                return RedirectToAction("ReviewArticle", "ArticleReviews", new { id = createCommentViewModel.ArticleId });
            }
            return RedirectToAction("DetailArticle", new { Id = createCommentViewModel.ArticleId });

        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.Categories = await _categoryService.GetAllCategories();
                return View();
            }
            return RedirectToAction("Articles");
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateArticleViewModel createArticleViewModel, EArticleMode articleMode)
        {
            createArticleViewModel.ArticleMode = articleMode;
            if (!ModelState.IsValid)
            {
                ViewBag.Categories = await _categoryService.GetAllCategories();
                return View(createArticleViewModel);
            }
            int? articleId = await _articleService.Create(createArticleViewModel);
            if (articleId is null)
            {
                return View(createArticleViewModel);
            }

            if (createArticleViewModel.ArticleMode == EArticleMode.UnderReview)
            {
                await _articleReviewService.SendReviewToModerators(articleId);
            }
            
            return RedirectToAction("Articles");
        }

        [HttpGet]
        public async Task<IActionResult> CreateArticleFromDraft(int articleId)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.Categories = await _categoryService.GetAllCategories();
                var article = await _articleService.GetArticle(articleId);
                CreateArticleViewModel createArticleViewModel = new CreateArticleViewModel()
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    Description = article.Description,
                    CategoryId = article.CategoryId,
                    AuthorId = article.AuthorId
                };
                return View(createArticleViewModel);
            }
            return RedirectToAction("Articles");
        }

        [HttpPost]
        public async Task<IActionResult> CreateArticleFromDraft(CreateArticleViewModel createArticleViewModel, EArticleMode articleMode)
        {
            createArticleViewModel.ArticleMode = articleMode;
            if (!ModelState.IsValid)
            {
                ViewBag.Categories = await _categoryService.GetAllCategories();
                return View(createArticleViewModel);
            }

            await _articleService.DeleteArticle(createArticleViewModel.Id);
            int? articleId = await _articleService.Create(createArticleViewModel);

            if (createArticleViewModel.ArticleMode == EArticleMode.UnderReview)
            {
                await _articleReviewService.SendReviewToModerators(articleId);
            }

            return RedirectToAction("UserProfile", "Account", new {userName = User.Identity.Name});
        }

        [HttpGet]
        public IActionResult ToPublishArticle()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ToPublishArticle(CreatePublishViewModel model)
        {
            await _articleService.ToPublish(model);
            return RedirectToAction("DetailArticle", new { id = model.ArticleId });
        }

        [HttpGet]
        public async Task<IActionResult> AddArticleToDraft(int articleId)
        {
            await _articleService.ToDraft(articleId);
            return RedirectToAction("Articles");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id, string userName)
        {
            await _articleService.DeleteArticle(id);
            return RedirectToAction("UserProfile", "Account", new { userName = userName });
        }

    }
}
