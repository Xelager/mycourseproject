﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.Comment;
using GeekStreamProject.Core.ViewModels.Review;
using GeekStreamProject.ENums.ENums;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace GeekStreamProject.App.Controllers
{
    public class ArticleReviewsController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ICategoryService _categoryService;
        private readonly ICommentService _commentService;
        private readonly IArticleReviewService _articleReviewService;

        public ArticleReviewsController(IArticleService articleService, ICategoryService categoryService,
            UserManager<ApplicationUser> userManager, ICommentService commentService, IArticleReviewService articleReviewService)
        {
            _articleService = articleService;
            _categoryService = categoryService;
            _userManager = userManager;
            _commentService = commentService;
            _articleReviewService = articleReviewService;
        }

        
        [HttpGet]
        public async Task<IActionResult> ReviewArticle(int id)
        {
            var reviewArticleViewModel = await _articleReviewService.GetReviewArticle(id);
            if (reviewArticleViewModel == null)
            {
                return NotFound();
            }
            
            return View(reviewArticleViewModel);
        }

        [HttpGet]
        public IActionResult ApproveArticle()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ApproveArticle(CreateApproveViewModel model, int articleReviewId)
        {
            await  _articleReviewService.ApproveArticle(model);
            var articleByReview = await _articleService.GetArticleByReview(articleReviewId);
            if (articleByReview is null)
            {
                return NotFound();
            }
            return RedirectToAction("ReviewArticle", new { id = articleByReview.Id });
        }

        [HttpGet]
        public async Task<IActionResult> UnApprove(int approveId, int articleReviewId)
        {
            await _articleReviewService.UnApprove(approveId);
            var articleByReview = await _articleService.GetArticleByReview(articleReviewId);
            if (articleByReview is null)
            {
                return NotFound();
            }
            return RedirectToAction("ReviewArticle", new { id = articleByReview.Id });
        }
    }
}
