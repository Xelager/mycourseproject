﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels.Chat;
using Microsoft.AspNetCore.Mvc;

namespace GeekStreamProject.App.Controllers
{
    public class ChatController : Controller
    {
        private readonly IChatService _chatService;

        public ChatController(IChatService chatService)
        {
            _chatService = chatService;
        }

        public IActionResult _LayoutChat()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Chat()
        {
            IEnumerable<MessageViewModel> chatMessages = await _chatService.GetLatestMessages();

            if (chatMessages == null)
            {
                return NotFound();
            }

            return View(chatMessages);
        }
    }
}
