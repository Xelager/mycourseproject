﻿using System;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using GeekStreamProject.Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.ViewModels.User;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using GeekStreamProject.Core.Abstractions.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using GeekStreamProject.ENums.ENums;
using GeekStreamProject.Core.ViewModels.Image;
using System.Collections.Generic;
using System.Linq;
using GeekStreamProject.Core.ViewModels.Article;

namespace GeekStreamProject.App.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUsersRepository _userRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IImageService _imageService;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly IImageRepository _imageRepository;
        private readonly IArticleService _articleService;
        private readonly ISubscriptionService _subscriptionService;
        private readonly IArticleReviewService _articleReviewService;

        public AccountController(UserManager<ApplicationUser> userManager
            ,SignInManager<ApplicationUser> signInManager, IImageService imageService, IUsersRepository userRepository,
            IUserService userService, IEmailService emailService, IImageRepository imageRepository, IArticleService articleService,
            ISubscriptionService subscriptionService, IArticleReviewService articleReviewService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _imageService = imageService;
            _userRepository = userRepository;
            _userService = userService;
            _emailService = emailService;
            _imageRepository = imageRepository;
            _articleService = articleService;
            _subscriptionService = subscriptionService;
            _articleReviewService = articleReviewService;
        }

        [AllowAnonymous]
        public IActionResult ConfirmPage() => View();

        [AllowAnonymous]
        public IActionResult UpdatePasswordSuccess() => View();

        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation() => View();

        [AllowAnonymous]
        public IActionResult ForAnonymUser() => View();

        public IActionResult ForModeratorUser() => View();

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Subscriptions(string userId)
        {
            var listOfSubscriptions = await _subscriptionService.SubscriptionsUser(userId);
            var listOfAuthor = listOfSubscriptions.Select(sub => new AuthorViewModel()
            {
                Id = sub.authorViewModel.Id,
                FirstName = sub.authorViewModel.FirstName,
                LastName = sub.authorViewModel.LastName,
                UserName = sub.authorViewModel.UserName,
                Rating = sub.authorViewModel.Rating,
                AvatarPath = sub?.authorViewModel.AvatarPath,
                CountArticle = sub.authorViewModel.CountArticle,
                articleViewModel = sub.authorViewModel.articleViewModel

            });
            return View(listOfAuthor);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Subscribers(string userId)
        {
            var listOfSubscribers = await _subscriptionService.SubscribersUser(userId);
            var listOfAuthor = listOfSubscribers.Select(sub => new AuthorViewModel()
            {
                Id = sub.subscriberViewModel.Id,
                FirstName = sub.subscriberViewModel.FirstName,
                LastName = sub.subscriberViewModel.LastName,
                UserName = sub.subscriberViewModel.UserName,
                Rating = sub.subscriberViewModel.Rating,
                AvatarPath = sub?.subscriberViewModel.AvatarPath,
                CountArticle = sub.subscriberViewModel.CountArticle,
                articleViewModel = sub.subscriberViewModel.articleViewModel

            });
            return View(listOfAuthor);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> AuthorArticles(string userId)
        {
            var articleViewModels = await _articleService.GetArticlesByAuthor(userId);
            if (articleViewModels == null)
            {
                return NotFound();
            }
            return View(articleViewModels);
        }

        [HttpGet]
        public async Task<IActionResult> Drafts(string authorId)
        {
            var articleViewModels = await _articleService.GetMyDrafts(authorId);
            if (articleViewModels == null)
            {
                return NotFound();
            }
            return View(articleViewModels);
        }


        [AllowAnonymous]
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model is null)
            {
                return View(model);
            }
            model.Avatar = await _imageService.InstallDefaultAvatar();
            bool result = await _userService.Register(model,
                (token, id) =>
                    Url.Action("ConfirmAccount", "Account", new { @token = token, @userId = id },
                        Request.Scheme, null));

            if (!result)
            {
                ModelState.AddModelError("Error", "Данные введены некорректно");
                return View(model);
            }

            return RedirectToAction("ConfirmPage");
        }

        [AllowAnonymous]
        public async Task<IActionResult> ConfirmAccount(string token, string userId)
        {
            bool confirmResult = await _emailService.ConfirmAccount(token, userId);
            if (!confirmResult)
            {
                return Challenge();
            }

            return View("SuccessConfirm");
        }


        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            var model = new LoginViewModel()
            {
                ReturnUrl = returnUrl,
                ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList()
            };
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (User.Identity.IsAuthenticated)
            {
                ModelState.AddModelError("IsAuth", "Пользователь авторизован");
                return View(model);
            }

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
                throw new ArgumentException("Пользователь не должен быть равен нулю");

            var signInResult = await _signInManager.PasswordSignInAsync(user, model.Password, false, false);

            if (!signInResult.Succeeded)
            {
                ModelState.AddModelError("Error", "Неверный логин или пароль");
                return View(model);
            }

            var userProfile = await _userRepository.GetUserProfile(user.UserName);
            if (userProfile.Avatar != null)
            {
                var claim = new Claim("avatarUrl", userProfile.Avatar?.FilePath);
                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                identity.AddClaim(claim);
            }
            return Redirect(Url.Action("Articles", "Articles"));
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult>
            ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            LoginViewModel loginViewModel = new LoginViewModel
            {
                ReturnUrl = returnUrl,
                ExternalLogins =
                        (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList()
            };

            if (remoteError != null)
            {
                ModelState
                    .AddModelError(string.Empty, $"Error from external provider: {remoteError}");

                return View("Login", loginViewModel);
            }

            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ModelState
                    .AddModelError(string.Empty, "Error loading external login information.");

                return View("Login", loginViewModel);
            }

            var signInResult = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider,
                info.ProviderKey, isPersistent: false, bypassTwoFactor: true);

            if (signInResult.Succeeded)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);

                if (email != null)
                {
                    var user = await _userManager.FindByEmailAsync(email);

                    if (user == null)
                    {
                        ImageViewModel avatar = await _imageService.InstallDefaultAvatar();
                        user = new ApplicationUser
                        {
                            UserName = info.Principal.FindFirstValue(ClaimTypes.Email),
                            Email = info.Principal.FindFirstValue(ClaimTypes.Email),
                            AvatarId = avatar?.Id,
                            FirstName = info.Principal.FindFirstValue(ClaimTypes.GivenName),
                            LastName = info.Principal.FindFirstValue(ClaimTypes.Surname),
                            Rating = 0,
                            DateOfCreation = DateTime.Now,
                            EmailConfirmed = true
                        };

                        var result = await _userManager.CreateAsync(user);
                        if (!result.Succeeded)
                        {
                            return View("Login", loginViewModel);
                        }
                        await _userManager.AddToRoleAsync(user, "User");
                    }

                    // Add a login (i.e insert a row for the user in AspNetUserLogins table)
                    await _userManager.AddLoginAsync(user, info);
                    await _signInManager.SignInAsync(user, isPersistent: false);

                    return LocalRedirect(returnUrl);
                }

                // If we cannot find the user email we cannot continue
                ViewBag.ErrorTitle = $"Email claim not received from: {info.LoginProvider}";
                ViewBag.ErrorMessage = "Please contact support on geekstream@support.com";

                return View("Error");
            }
        }

        [HttpGet]
        public async Task<IActionResult> UserSignOut()
        {
            if (User?.Identity?.IsAuthenticated == true)
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction("Articles", "Articles"); ;
            }

            return RedirectToAction("Articles", "Articles");
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Authors(string userName)
        {
            var listOfAuthors = await _userService.GetAllUsers();

            if (listOfAuthors is null)
            {
                return NotFound();
            }

            return View(listOfAuthors);
        }

        [HttpGet]
        public async Task<IActionResult> UserProfile(string userName)
        {
            var userProfile = await _userService.GetUser(userName);
            if (userProfile is null)
            {
                return NotFound();
            }

            return View(userProfile);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool result = await _userService.ResetPassword(model,
                    (token, id) =>
                        Url.Action("UpdatePassword", "Account", new { @token = token, @userId = id },
                            Request.Scheme, null));
                
                return View("ForgotPasswordConfirmation");
            }

            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> UpdatePassword(string token, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user is null)
            {
                return NotFound();
            }
            return View(new UpdatePasswordViewModel { token = token, userId = user.Id});
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> UpdatePassword(UpdatePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            bool result = await _userService.UpdatePassword(model);

            if (!result)
            {
                ModelState.AddModelError("Error", "Данные введены некорректно");
                return View(model);
            }

            return RedirectToAction("UpdatePasswordSuccess");
        }


     
        [HttpGet]
        public IActionResult UpdateAvatar()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateAvatar(ChangeAvatarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            var user = User.FindFirstValue(ClaimTypes.NameIdentifier);
            model.Id = user;
            await _userService.UpdateAvatar(model);
            return RedirectToAction("UserProfile", new { userName = User.Identity.Name });
        }

        
        [HttpGet]
        public async Task<IActionResult> Reviews()
        {
            var reviewArticleViewModels = await _articleReviewService.GetAllReviewArticles();
            if (reviewArticleViewModels == null)
            {
                return NotFound();
            }
            return View(reviewArticleViewModels);
        }

        [HttpGet]
        public async Task<IActionResult> MyReviews(string authorId)
        {
            var reviewArticleViewModels = await _articleReviewService.GetMyReviewArticles(authorId);
            if (reviewArticleViewModels == null)
            {
                return NotFound();
            }
            return View(reviewArticleViewModels);
        }

        [HttpGet]
        public async Task<IActionResult> NotPublishedArticles(string authorId)
        {
            var articleViewModels = await _articleService.GetMyNotPublishedArticles(authorId);
            if (articleViewModels == null)
            {
                return NotFound();
            }
            return View(articleViewModels);
        }
    }
}
