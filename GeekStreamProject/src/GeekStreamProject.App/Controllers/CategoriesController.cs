﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.ViewModels.Category;
using AutoMapper.Configuration;
using GeekStreamProject.Core.Abstractions.Services;

namespace GeekStreamProject.App.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;
        private IArticleService _articleService;

        public CategoriesController(ICategoryService categoryService, IArticleService articleService)
        {
            _categoryService = categoryService;
            _articleService = articleService;
        }

        [HttpGet]
        public async Task<IActionResult> Categories()
        {
            var categories = await _categoryService.GetAllCategories();

            if (categories == null)
            {
                return NotFound();
            }
            return View(categories);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreateCategoryViewModel model)
        {
            await _categoryService.Create(model);
            return RedirectToAction("Categories");
        }

        [HttpGet] 
        public async Task<IActionResult> ArticlesByCategory(int id)
        {
            var articleWithCategory = await _articleService.GetArticlesByCategory(id);
            if (articleWithCategory == null)
            {
                return RedirectToAction("Categories");
            }
            return View(articleWithCategory);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            await _categoryService.DeleteCategory(id);
            return RedirectToAction("Categories");
        }
    }
}
