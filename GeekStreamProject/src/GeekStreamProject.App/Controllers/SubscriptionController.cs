﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.ViewModels.Subscription;
using Microsoft.AspNetCore.Mvc;

namespace GeekStreamProject.App.Controllers
{
    public class SubscriptionController : Controller
    {
        private readonly ISubscriptionService _subscriptionService;

        public SubscriptionController(ISubscriptionService subscriptionService)
        {
            _subscriptionService = subscriptionService;
        }

        [HttpGet]
        public IActionResult SubscribeToAuthor()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SubscribeToAuthor(CreateSubscriptionViewModel model)
        {
            await _subscriptionService.SubscribeToAuthor(model);
            return RedirectToAction("UserProfile", "Account", new { userName = model.AuthorName });
        }

        [HttpGet]
        public IActionResult SubscribeToCategory()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SubscribeToCategory(CreateSubscriptionViewModel model, int articleId)
        {
            await _subscriptionService.SubscribeToCategory(model);
            return RedirectToAction("ArticlesByCategory", "Categories", articleId);
        }

        [HttpGet]
        public async Task<IActionResult> Unsubscribe(int subId, string userName)
        {
            await _subscriptionService.Unsubscribe(subId);
            return RedirectToAction("UserProfile", "Account", new { userName = userName });
        }
    }
}
