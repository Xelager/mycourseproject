﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.ViewModels;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.ViewModels.Comment;
using Microsoft.AspNetCore.Identity;

namespace GeekStreamProject.App.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }


        [HttpGet]
        public async Task<IActionResult> DeleteComment(int id, int articleId)
        {
            await _commentService.DeleteComment(id);
            return RedirectToAction("DetailArticle", "Articles", new { Id = articleId });
        }

    }
}
