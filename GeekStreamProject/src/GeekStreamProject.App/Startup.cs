using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Infrastructure.DataAccess.AppDbContext;
using GeekStreamProject.Infrastructure.DataAccess.Repositories;
using Microsoft.AspNetCore.Http;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared;
using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Abstractions;
using GeekStreamProject.Core.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.IO;
using GeekStreamProject.App.Models;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.AspNetCore.Authentication;
using GeekStreamProject.App.Hubs;
using FluentValidation;
using GeekStreamProject.Core.Abstractions.Services;
using GeekStreamProject.Core.Validations;
using GeekStreamProject.Core.ViewModels.Article;
using GeekStreamProject.Core.ViewModels.User;
using Microsoft.AspNetCore.Identity;

namespace GeekStreamProject.App
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
            if (!Directory.Exists(_configuration["ImageStorage"]))
            {
                Directory.CreateDirectory(_configuration["ImageStorage"]);
            }
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddHttpContextAccessor()
                .AddControllersWithViews()
                .AddFluentValidation();

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
                {
                    options.Password.RequiredLength = 8;
                    options.Password.RequiredUniqueChars = 3;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireDigit = false;
                    options.User.RequireUniqueEmail = true;
                    options.SignIn.RequireConfirmedEmail = true;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(_configuration.GetConnectionString("DefaultConnection")));

            services
                .AddScoped<IArticleRepository, ArticleRepository>()
                .AddScoped<ICommentRepository, CommentRepository>()
                .AddScoped<ISubscriptionRepository, SubscriptionRepository>()
                .AddScoped<ICategoryRepository, CategoryRepository>()
                .AddScoped<IImageRepository, ImageRepository>()
                .AddScoped<IUsersRepository, UsersRepository>()
                .AddScoped<IChatRepository, ChatRepository>()
                .AddScoped<IChatMessageRepository, ChatMessageRepository>()
                .AddScoped<IArticleReviewRepository, ArticleReviewRepository>()
                .AddScoped<IArticleReviewerUserRepository, ArticleReviewerUserRepository>()
                .AddScoped<IApproveRepository, ApproveRepository>();

            services
                .AddScoped<IArticleService, ArticleService>()
                .AddScoped<ICategoryService, CategoryService>()
                .AddScoped<IImageService, ImageService>()
                .AddScoped<ICommentService, CommentService>()
                .AddScoped<IChatService, ChatService>()
                .AddScoped<IUserService, UserService>()
                .AddScoped<ISubscriptionService, SubscriptionService>()
                .AddScoped<IEmailService, EmailService>()
                .AddScoped<IArticleReviewService, ArticleReviewService>();

            services
                .AddTransient<IValidator<LoginViewModel>, LoginViewModelValidator>()
                .AddTransient<IValidator<RegisterViewModel>, RegisterViewModelValidator>()
                .AddTransient<IValidator<ForgotPasswordViewModel>, ForgotPasswordViewModelValidator>()
                .AddTransient<IValidator<UpdatePasswordViewModel>, ResetPasswordViewModelValidator>()
                .AddTransient<IValidator<ChangeAvatarViewModel>, ChangeAvatarModelValidator>()
                .AddTransient<IValidator<CreateArticleViewModel>, ArticleViewModelValidator>();

            services.AddScoped<IClaimsTransformation, AddAvatarClaimsTransformation>();

            services.AddSignalR();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => //CookieAuthenticationOptions
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                })
                .AddGoogle(options =>
                {
                    options.ClientId = "551038018513-8autokqgsopnctbo7g7lsosgmo3tdrbc.apps.googleusercontent.com";
                    options.ClientSecret = "PT8Jl_So2f0IoZKFz8e_1-sV";
                });
            services.AddRazorPages();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                using var scope = app.ApplicationServices.CreateScope();
                var res = new UserSeed(scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>(),
                        _configuration,
                        scope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>())
                    .Seed();
                res.Wait();

            }
            else
            {
                app.UseExceptionHandler("/Articles/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "Login",
                    pattern: "/Login",
                    defaults: new { Controller = "Account", Action = "Login" });

                endpoints.MapControllerRoute(
                    name: "Register",
                    pattern: "/Register",
                    defaults: new { Controller = "Account", Action = "Register" });

                endpoints.MapControllerRoute(
                    name: "Categories",
                    pattern: "/Categories",
                    defaults: new { Controller = "Categories", Action = "Categories" });

                endpoints.MapControllerRoute(
                    name: "CreateCategories",
                    pattern: "/Categories/Create",
                    defaults: new { Controller = "Categories", Action = "Create" });

                endpoints.MapControllerRoute(
                    name: "DetailArticle",
                    pattern: "/Articles/post/{id?}",
                    defaults: new { Controller = "Articles", Action = "DetailArticle" });

                endpoints.MapControllerRoute(
                    name: "CreateArticleFromDraft",
                    pattern: "/Articles/draft/",
                    defaults: new { Controller = "Articles", Action = "CreateArticleFromDraft" });

                endpoints.MapControllerRoute(
                    name: "AccountProfile",
                    pattern: "/UserProfile/{userName?}",
                    defaults: new { Controller = "Account", Action = "UserProfile" });

                endpoints.MapControllerRoute(
                    name: "ArticleByCategory",
                    pattern: "/Categories/{id?}",
                    defaults: new { Controller = "Categories", Action = "ArticlesByCategory" });

                endpoints.MapControllerRoute(
                    name: "Authors",
                    pattern: "/Authors",
                    defaults: new { Controller = "Account", Action = "Authors" });

                endpoints.MapControllerRoute(
                    name: "ConfirmPage",
                    pattern: "/ConfirmPage",
                    defaults: new { Controller = "Account", Action = "ConfirmPage" });

                endpoints.MapControllerRoute(
                    name: "ReviewArticle",
                    pattern: "/ReviewArticle/review/{id?}",
                    defaults: new { Controller = "ArticleReviews", Action = "ReviewArticle" });


                endpoints.MapControllerRoute(
                    name: "",
                    pattern: "{controller=Articles}/{action=Articles}/{id?}");

                endpoints.MapHub<ChatHub>("/chat");
            });
        }
    }
}
